<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promotion extends CI_Controller
{

    public function index ()
    {
        session_start();
        $this->load->view('/Promotion/index');
    }

    public function Create ()
    {
        session_start();
        // 載入頁面
        $this->load->view('/Promotion/Create');
        
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Name = $_POST["Name"];
            $Context = $_POST["Context"];
            $BeaconId = $_POST["BeaconId"];
            $Shop = $_POST["Shop"];
            
            $SalePrice = $_POST["SalePrice"];
            $ShopId = $_POST["ShopId"];
            $Note = $_POST["Note"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "INSERT INTO `promotions` (`Id`, `Name`, `Context`, `BeaconId`, `Shop`, `SalePrice`, `ShopId`, `TIME`)
			VALUES (NULL, '$Name', '$Context', '$BeaconId', '$Shop', '$SalePrice', '$ShopId', '$TIME')";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"新增優惠資料成功\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Promotion>';
            } else {
                echo "<script>";
                echo "alert(\"新增資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Promotion>';
            }
            
            $conn->close();
        }
    }

    public function Edit ()
    {
        session_start();
        // 載入頁面
        $this->load->view('/Promotion/Edit');
    }

    public function Update ()
    {
        session_start();
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            $Name = $_POST["Name"];
            $Context = $_POST["Context"];
            $BeaconId = $_POST["BeaconId"];
            // $Shop = $_POST["Shop"];
            $SalePrice = $_POST["SalePrice"];
            $ShopId = $_POST["ShopId"];
            $TIME = $_POST["TIME"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "UPDATE `promotions` SET `Name` = '$Name', `Context` = '$Context', `BeaconId` = '$BeaconId', `SalePrice` = '$SalePrice', 
            `ShopId` = '$ShopId', `TIME` = '$TIME' WHERE `promotions`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"修改優惠資料成功\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Promotion>';
            } else {
                echo "<script>";
                echo "alert(\"修改資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Promotion>';
            }
            
            $conn->close();
        }
    }

    public function Delete ()
    {
        session_start();
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "DELETE FROM `promotions` WHERE `promotions`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"刪除優惠資料成功\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Promotion>';
            } else {
                echo "<script>";
                echo "alert(\"刪除資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Promotion>';
            }
            
            $conn->close();
        }
    }
}
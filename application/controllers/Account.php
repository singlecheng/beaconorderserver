<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends CI_Controller
{

    public function index ()
    {
        session_start();
        $this->load->view('/Account/login');
    }

    public function view ()
    {
        $this->load->database();
        session_start();
        
        if(isset($_SESSION['username'])){
            if ($_SESSION ['username'] == "admin") {
                $this->load->view('/Account/View');
            }
            else{
                $id = $_SESSION ['username'];
                $sql = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject From Account', CURRENT_TIMESTAMP)";
                $this->db->query($sql);
                echo 'Access Denied';
                echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php>';
            }
        }
        else {
            echo 'Please Login';
	        echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php/Account/index>';
        }
    }

    public function Create ()
    {
        session_start();
        $this->load->view('/Account/Create');
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Accountname = $_POST["Accountname"];
            $Accountpassword = $_POST["Accountpassword"];
            $role = $_POST["role"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES ('', '$Accountname', '$Accountpassword', '$role')";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"Account Created Success\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Account/view>';
            } else {
                echo "<script>";
                echo "alert(\"Account Create failed\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Account/create>';
            }
            
            $conn->close();
        }
    }

    public function Edit ()
    {}

    public function Log ()
    {
        session_start();
        $this->load->view('/Account/Log');
    }

    public function connect ()
    {
        session_start();
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $id = $_POST['id'];
        $pw = $_POST['pw'];
        
        // 搜尋資料庫資料
        $sql = "SELECT * FROM `users` WHERE username = '$id'";
        $result = mysqli_query($conn, $sql);
        $row = @mysqli_fetch_row($result);
        
        // 判斷帳號與密碼是否為空白
        // 以及MySQL資料庫裡是否有這個會員
        if ($id != null && $pw != null && $row[1] == $id && $row[2] == $pw) {
            // 將帳號寫入session，方便驗證使用者身份
            $_SESSION['username'] = $id;
            $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Login', CURRENT_TIMESTAMP)";
            $conn->query($sql2) === TRUE;
            echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer>';
        } else {
            
            $sql3 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Login Failed', CURRENT_TIMESTAMP)";
            $conn->query($sql3) === TRUE;
            echo 'Access Denied';
            echo '<meta http-equiv=REFRESH CONTENT=1;url=index>';
        }
    }

    public function logout ()
    {
        session_start();
        $id = $_SESSION['username'];
        
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $sql4 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Logout', CURRENT_TIMESTAMP)";
        $conn->query($sql4) === TRUE;
        unset($_SESSION['username']);
        echo 'Logout . . . . . . ';
        echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer>';
    }
}
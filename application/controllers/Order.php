<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{

    public function index ()
    {
        $page = $_SERVER ['PHP_SELF'];
        $sec = "7";
        
        $this->load->database();
        session_start();
        $this->load->model('OrderModel');
        $data['OrderGroupCount'] = $this->OrderModel->GetOrderGroupCount();
        $data['OrderGrouplist'] = $this->OrderModel->GetOrderGroupList();
        $data['page'] = $page;
        $data['sec'] = $sec;
        if(isset($_SESSION['username'])){
            if ($_SESSION ['username'] == "order" or $_SESSION ['username'] == "admin") {
                $this->load->view('/Order/index', $data);
            }
            else{
                $id = $_SESSION ['username'];
                $sql = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject From Order', CURRENT_TIMESTAMP)";
                $this->db->query($sql);
                echo 'Access Denied';
                echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php>';
            }
        }
        else {
            echo 'Please Login';
	        echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php/Account/index>';
        }
        
    }

    public function Create ()
    {
        session_start();
        $this->load->view('/Order/Create');
        
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Shop = $_POST["Shop"];
            $Product = $_POST["Product"];
            $Count = $_POST["Count"];
            $Price = $_POST["Price"];
            $Purchase = $_POST["Purchase"];
            $CustomerEmail = $_POST["CustomerEmail"];
            $BeaconId = $_POST["BeaconId"];
            $ProductId = $_POST["ProductId"];
            $ShopId = $_POST["ShopId"];
            $CustomerId = $_POST["CustomerId"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "INSERT INTO `orderlist` (`Id`, `Shop`, `Product`, `Count`, `Price`, `CeateTime`, `Purchase`, `CustomerEmail`, `BeaconId`, `ProductId`, `ShopId`, `CustomerId`) 
			VALUES (NULL, '$Shop', '$Product', '$Count', '$Price', CURRENT_TIMESTAMP, '$Purchase', '$CustomerEmail', '$BeaconId', '$ProductId', '$ShopId', '$CustomerId')";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"Create Success\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order';
            } else {
                echo "<script>";
                echo "alert(\"Create Failed\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order/Create>';
            }
            
            $conn->close();
        }
    }

    public function Detail ()
    {
        session_start();
        $this->load->view('/Order/Detail');
    }

    public function Edit ()
    {
        session_start();
        $this->load->view('/Order/Edit');
    }

    public function UpdateGroup ()
    {
        session_start();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            $Complete = $_POST["Complete"];
            $Purchase = $_POST["Purchase"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "UPDATE `ordergroup` SET `Purchase` = '$Purchase', `Complete` = '$Complete' WHERE `ordergroup`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<link rel='stylesheet' href='/BeaconOrderServer/assets/css/bootstrap.min.css'>
                      <script src='/BeaconOrderServer/assets/js/jquery-1.11.3.min.js'></script>
                      <script src='/BeaconOrderServer/assets/js/bootstrap.min.js'></script>
                      <div class='alert alert-success'>
                        <strong>Success ! </strong> 
                      </div>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order>';
            } else {
                echo "<script>";
                echo "alert(\"Edit Failed\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order>';
            }
            
            $conn->close();
        }
        
        // Log處理
        $id = $_SESSION['username'];
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'UpdateGroup', CURRENT_TIMESTAMP)";
        $conn->query($sql2) === TRUE;
    }

    public function Update ()
    {
        session_start();
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            $Complete = $_POST["Complete"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "UPDATE `orderlist` SET `Complete` = '$Complete' WHERE `orderlist`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<link rel='stylesheet' href='/BeaconOrderServer/assets/css/bootstrap.min.css'>
                      <script src='/BeaconOrderServer/assets/js/jquery-1.11.3.min.js'></script>
                      <script src='/BeaconOrderServer/assets/js/bootstrap.min.js'></script>
                      <div class='alert alert-success'>
                        <strong>Success ! </strong> 
                      </div>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order>';
            } else {
                echo "Edit Failed";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order/Edit>';
            }
            
            $conn->close();
        }
        // Log處理
        $id = $_SESSION['username'];
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'UpdateDetail', CURRENT_TIMESTAMP)";
        $conn->query($sql2) === TRUE;
    }

    public function Delete ()
    {
        session_start();
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "DELETE FROM `orderlist` WHERE `orderlist`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<link rel='stylesheet' href='/BeaconOrderServer/assets/css/bootstrap.min.css'>
                      <script src='/BeaconOrderServer/assets/js/jquery-1.11.3.min.js'></script>
                      <script src='/BeaconOrderServer/assets/js/bootstrap.min.js'></script>
                      <div class='alert alert-success'>
                        <strong>Delete Success ! </strong> 
                      </div>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order>';
            } else {
                echo "<script>";
                echo "alert(\"刪除資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Order>';
            }
            
            $conn->close();
        }
        
        // Log處理
        $id = $_SESSION['username'];
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bryan_beaconorder";
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        
        $sql2 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'DeleteOrder', CURRENT_TIMESTAMP)";
        $conn->query($sql2) === TRUE;
    }
}
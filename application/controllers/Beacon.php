<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beacon extends CI_Controller
{

    public function index ()
    {
        $this->load->database();
        session_start();
        
        if(isset($_SESSION['username'])){
            if ($_SESSION ['username'] == "bea" or $_SESSION ['username'] == "admin") {
                $this->load->view('/Beacon/index');
            }
            else{
                $id = $_SESSION ['username'];
                $sql = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject From Beacon', CURRENT_TIMESTAMP)";
                $this->db->query($sql);
                echo 'Access Denied';
                echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php>';
            }
        }
        else {
            echo 'Please Login';
	        echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php/Account/index>';
        }
    }

    public function Create ()
    {
        session_start();
        $this->load->view('/Beacon/Create');
        
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Major = $_POST["Major"];
            $Minor = $_POST["Minor"];
            $Name = $_POST["Name"];
            $Locate = $_POST["Locate"];
            $Note = $_POST["Note"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "INSERT INTO `beacon` (`Id`, `Major`, `Minor`, `Name`, `Locate`, `Note`) VALUES (NULL, '$Major', '$Minor', '$Name', '$Locate', '$Note')";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"新增Beacon成功\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Beacon>';
            } else {
                echo "<script>";
                echo "alert(\"新增Beacon資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Beacon/Create>';
            }
            
            $conn->close();
        }
    }

    public function Edit ()
    {
        session_start();
        $this->load->view('/Beacon/Edit');
    }
}

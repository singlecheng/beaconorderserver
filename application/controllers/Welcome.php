<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function index ()
    {
        session_start();
        $this->load->view('welcome_index');
    }
}
?>
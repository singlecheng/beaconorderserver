<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shop extends CI_Controller
{

    public function index ()
    {
        session_start();
        $this->load->view('/Shop/index');
    }

    public function Create ()
    {
        session_start();
        $this->load->view('/Shop/Create');
        
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Name = $_POST["Name"];
            $Locate = $_POST["Locate"];
            $Capacity = $_POST["Capacity"];
            $Area = $_POST["Area"];
            $BeaconUse = $_POST["BeaconUse"];
            $MenuId = $_POST["MenuId"];
            $Person = $_POST["Person"];
            $Contact = $_POST["Contact"];
            $Note = $_POST["Note"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "INSERT INTO `shop` (`Id`, `Name`, `Locate`, `Capacity`, `Area`, `BeaconUse`, `MenuId`, `Person`, `Contact`, `Note`) 
                    VALUES (NULL, '$Name', '$Locate', '$Capacity', '$Area', '$BeaconUse', '$MenuId', '$Person', '$Contact', '$Note')";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"新增Shop成功\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Shop';
            } else {
                echo "<script>";
                echo "alert(\"新增Shop資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Shop/Create>';
            }
            
            $conn->close();
        }
    }

    public function Edit ()
    {
        session_start();
        $this->load->view('/Shop/Edit');
    }

    public function Update ()
    {
        session_start();
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            $Name = $_POST["Name"];
            $Locate = $_POST["Locate"];
            $Capacity = $_POST["Capacity"];
            $Area = $_POST["Area"];
            $BeaconUse = $_POST["BeaconUse"];
            $MenuId = $_POST["MenuId"];
            $Person = $_POST["Person"];
            $Contact = $_POST["Contact"];
            $Note = $_POST["Note"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "UPDATE `shop` SET `Name` = '$Name', `Locate` = '$Locate', `Capacity` = '$Capacity', `Area` = '$Area', `BeaconUse` = '$BeaconUse', `MenuId` = '$MenuId', 
            `Person` = '$Person', `Contact` = '$Contact', `Note` = '$Note' WHERE `shop`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"修改Shop資料成功\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Shop>';
            } else {
                echo "<script>";
                echo "alert(\"修改Shop資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Shop>';
            }
            
            $conn->close();
        }
    }

    public function Delete ()
    {
        session_start();
        // 檢測是否post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "bryan_beaconorder";
            
            $conn = new mysqli($servername, $username, $password, $dbname);
            mysqli_set_charset($conn, "utf8");
            
            $Id = $_POST["Id"];
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // 輸入字串編碼設UTF8
            mysqli_set_charset($conn, "utf8");
            
            $sql = "DELETE FROM `shop` WHERE `shop`.`Id` = $Id";
            
            if ($conn->query($sql) === TRUE) {
                echo "<script>";
                echo "alert(\"刪除shop資料成功\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Shop>';
            } else {
                echo "<script>";
                echo "alert(\"刪除shop資料失敗，請重新嘗試\");";
                echo "</script>";
                echo '<meta http-equiv=REFRESH CONTENT=1;url=/BeaconOrderServer/index.php/Shop>';
            }
            
            $conn->close();
        }
    }
}
<!DOCTYPE html>
<html>
<head>
<title>Data Analysis - Shop Report</title>
<meta charset="utf-8">

<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<div class="container">
		<div class="row">
        	<?php
									$servername = "localhost";
									$username = "root";
									$password = "";
									$dbname = "bryan_beaconorder";
									$dbc = "utf8";
									
									// Create connection
									$conn = new mysqli ( $servername, $username, $password, $dbname );
									mysqli_set_charset ( $conn, "utf8" );
									// Check connection
									if ($conn->connect_error) {
										die ( "Connection failed: " . $conn->connect_error );
									}
									
									$sql = "SELECT `Shop`, Count(*) FROM orderlist GROUP BY `ShopId`";
									$result = $conn->query ( $sql );
									if ($result->num_rows > 0) {
										?><table class="table table-striped">
				<thead>
					<tr>
						<th>Shop</th>
						<th>Order Count</th>
					</tr>
				</thead>
				<tbody><?php
										// output data of each row
										while ( $row = $result->fetch_assoc () ) {
											echo "<tr><td>" . $row ["Shop"] . "</td><td>" . $row ["Count(*)"] . "</td></tr>";
										}
										?></tbody>
			</table><?php
									} else {
										echo "0 results";
									}
									?>
        </div>

		<div class="row">
			<?php
			$sql = "SELECT Count(*) FROM `orderlist` WHERE `ShopId` = 1";
			$result = $conn->query ( $sql );
			if ($result->num_rows > 0) {
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num = $row ["Count(*)"];
					$row ["Count(*)"];
				}
			}
			
			$sql2 = "SELECT Count(*) FROM `orderlist` WHERE `ShopId` = 2";
			$result = $conn->query ( $sql2 );
			if ($result->num_rows > 0) {
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num2 = $row ["Count(*)"];
					$row ["Count(*)"];
				}
			}
			
			$sql3 = "SELECT Count(*) FROM `orderlist` WHERE `ShopId` = 3";
			$result = $conn->query ( $sql3 );
			if ($result->num_rows > 0) {
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num3 = $row ["Count(*)"];
					$row ["Count(*)"];
				}
			}
			
			$sql4 = "SELECT Count(*) FROM `orderlist` WHERE `ShopId` = 4";
			$result = $conn->query ( $sql4 );
			if ($result->num_rows > 0) {
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num4 = $row ["Count(*)"];
					$row ["Count(*)"];
				}
			}
			
			$sql5 = "SELECT Count(*) FROM `orderlist` WHERE `ShopId` = 5";
			$result = $conn->query ( $sql5 );
			if ($result->num_rows > 0) {
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num5 = $row ["Count(*)"];
					$row ["Count(*)"];
				}
			}
			?>
        </div>

		<!-- D3 -->
		<div class="row">
			<style>
body {
	width: 1060px;
	margin: 50px auto;
}

path {
	stroke: #fff;
}

path:hover {
	opacity: 0.9;
}

rect:hover {
	fill: blue;
}

.axis {
	font: 10px sans-serif;
}

.legend tr {
	border-bottom: 1px solid grey;
}

.legend tr:first-child {
	border-top: 1px solid grey;
}

.axis path, .axis line {
	fill: none;
	stroke: #000;
	shape-rendering: crispEdges;
}

.x.axis path {
	display: none;
}

.legend {
	margin-bottom: 76px;
	display: inline-block;
	border-collapse: collapse;
	border-spacing: 0px;
}

.legend td {
	padding: 4px 5px;
	vertical-align: bottom;
}

.legendFreq, .legendPerc {
	align: right;
	width: 50px;
}
</style>

			<body>
				<div id='dashboard'></div>
				<script src="http://d3js.org/d3.v3.min.js"></script>
				<script>
            function dashboard(id, fData){
                var barColor = 'steelblue';
                function segColor(c){ return {low:"#807dba", mid:"#e08214",high:"#41ab5d"}[c]; }
                
                // compute total for each state.
                fData.forEach(function(d){d.total=d.freq.low+d.freq.mid+d.freq.high;});
                
                // function to handle histogram.
                function histoGram(fD){
                    var hG={},    hGDim = {t: 60, r: 0, b: 30, l: 0};
                    hGDim.w = 500 - hGDim.l - hGDim.r, 
                    hGDim.h = 300 - hGDim.t - hGDim.b;
                        
                    //create svg for histogram.
                    var hGsvg = d3.select(id).append("svg")
                        .attr("width", hGDim.w + hGDim.l + hGDim.r)
                        .attr("height", hGDim.h + hGDim.t + hGDim.b).append("g")
                        .attr("transform", "translate(" + hGDim.l + "," + hGDim.t + ")");
            
                    // create function for x-axis mapping.
                    var x = d3.scale.ordinal().rangeRoundBands([0, hGDim.w], 0.1)
                            .domain(fD.map(function(d) { return d[0]; }));
            
                    // Add x-axis to the histogram svg.
                    hGsvg.append("g").attr("class", "x axis")
                        .attr("transform", "translate(0," + hGDim.h + ")")
                        .call(d3.svg.axis().scale(x).orient("bottom"));
            
                    // Create function for y-axis map.
                    var y = d3.scale.linear().range([hGDim.h, 0])
                            .domain([0, d3.max(fD, function(d) { return d[1]; })]);
            
                    // Create bars for histogram to contain rectangles and freq labels.
                    var bars = hGsvg.selectAll(".bar").data(fD).enter()
                            .append("g").attr("class", "bar");
                    
                    //create the rectangles.
                    bars.append("rect")
                        .attr("x", function(d) { return x(d[0]); })
                        .attr("y", function(d) { return y(d[1]); })
                        .attr("width", x.rangeBand())
                        .attr("height", function(d) { return hGDim.h - y(d[1]); })
                        .attr('fill',barColor)
                        .on("mouseover",mouseover)// mouseover is defined below.
                        .on("mouseout",mouseout);// mouseout is defined below.
                        
                    //Create the frequency labels above the rectangles.
                    bars.append("text").text(function(d){ return d3.format(",")(d[1])})
                        .attr("x", function(d) { return x(d[0])+x.rangeBand()/2; })
                        .attr("y", function(d) { return y(d[1])-5; })
                        .attr("text-anchor", "middle");
                    
                    function mouseover(d){  // utility function to be called on mouseover.
                        // filter for selected state.
                        var st = fData.filter(function(s){ return s.State == d[0];})[0],
                            nD = d3.keys(st.freq).map(function(s){ return {type:s, freq:st.freq[s]};});
                           
                        // call update functions of pie-chart and legend.    
                        pC.update(nD);
                        leg.update(nD);
                    }
                    
                    function mouseout(d){    // utility function to be called on mouseout.
                        // reset the pie-chart and legend.    
                        pC.update(tF);
                        leg.update(tF);
                    }
                    
                    // create function to update the bars. This will be used by pie-chart.
                    hG.update = function(nD, color){
                        // update the domain of the y-axis map to reflect change in frequencies.
                        y.domain([0, d3.max(nD, function(d) { return d[1]; })]);
                        
                        // Attach the new data to the bars.
                        var bars = hGsvg.selectAll(".bar").data(nD);
                        
                        // transition the height and color of rectangles.
                        bars.select("rect").transition().duration(500)
                            .attr("y", function(d) {return y(d[1]); })
                            .attr("height", function(d) { return hGDim.h - y(d[1]); })
                            .attr("fill", color);
            
                        // transition the frequency labels location and change value.
                        bars.select("text").transition().duration(500)
                            .text(function(d){ return d3.format(",")(d[1])})
                            .attr("y", function(d) {return y(d[1])-5; });            
                    }        
                    return hG;
                }
                
                // function to handle legend.
                function legend(lD){
                    var leg = {};
                        
                    // create table for legend.
                    var legend = d3.select(id).append("table").attr('class','legend');
                    
                    // create one row per segment.
                    var tr = legend.append("tbody").selectAll("tr").data(lD).enter().append("tr");
                        
                    // create the first column for each segment.
                    tr.append("td").append("svg").attr("width", '16').attr("height", '16').append("rect")
                        .attr("width", '16').attr("height", '16')
            			.attr("fill",function(d){ return segColor(d.type); });
                        
                    // create the second column for each segment.
                    tr.append("td").text(function(d){ return d.type;});
            
                    // create the third column for each segment.
                    tr.append("td").attr("class",'legendFreq')
                        .text(function(d){ return d3.format(",")(d.freq);});
            
                    // create the fourth column for each segment.
                    tr.append("td").attr("class",'legendPerc')
                        .text(function(d){ return getLegend(d,lD);});
            
                    // Utility function to be used to update the legend.
                    leg.update = function(nD){
                        // update the data attached to the row elements.
                        var l = legend.select("tbody").selectAll("tr").data(nD);
            
                        // update the frequencies.
                        l.select(".legendFreq").text(function(d){ return d3.format(",")(d.freq);});
            
                        // update the percentage column.
                        l.select(".legendPerc").text(function(d){ return getLegend(d,nD);});        
                    }
                    
                    function getLegend(d,aD){ // Utility function to compute percentage.
                        return d3.format("%")(d.freq/d3.sum(aD.map(function(v){ return v.freq; })));
                    }
            
                    return leg;
                }
                
                // calculate total frequency by segment for all state.
                var tF = ['low','mid','high'].map(function(d){ 
                    return {type:d, freq: d3.sum(fData.map(function(t){ return t.freq[d];}))}; 
                });    
                
                // calculate total frequency by state for all segment.
                var sF = fData.map(function(d){return [d.State,d.total];});
            
                var hG = histoGram(sF), // create the histogram.
                    pC = pieChart(tF), // create the pie-chart.
                    leg= legend(tF);  // create the legend.
            }
            </script>
				<script>
            var freqData=[
            {State:'韓鶴亭',freq:{low:<?php echo $num?>, mid:0, high:0}}
            ,{State:'GUGU廚房',freq:{low:<?php echo $num2?>, mid:0, high:0}}
            ,{State:'鮮五丼',freq:{low:<?php echo $num3?>, mid:0, high:0}}
            ,{State:'三商巧福',freq:{low:<?php echo $num4?>, mid:0, high:0}}
            ,{State:'翰林茶館',freq:{low:<?php echo $num5?>, mid:0, high:0}}
            ];
            dashboard('#dashboard',freqData);
            </script>
			</body>
		</div>

		<!-- Next -->
		<hr>
		<div class="row">
    		<?php
						$sql = "SELECT `Shop`, SUM(`Price`) AS Total FROM orderlist GROUP BY Shop";
						$result = $conn->query ( $sql );
						if ($result->num_rows > 0) {
							?><table class="table table-striped">
				<thead>
					<tr>
						<th>Shop</th>
						<th>Total Revenue</th>
					</tr>
				</thead>
				<tbody><?php
							// output data of each row
							while ( $row = $result->fetch_assoc () ) {
								echo "<tr><td>" . $row ["Shop"] . "</td><td>" . $row ["Total"] . "</td></tr>";
							}
							?></tbody>
			</table><?php
						} else {
							echo "0 results";
						}
						
						?>
<?php

						$sql = "SELECT `Shop`, SUM(`Price`) AS Total FROM orderlist WHERE ShopId = 1";
						$result = $conn->query ( $sql );
						if ($result->num_rows > 0) {
							// output data of each row
							while ( $row = $result->fetch_assoc () ) {
								$tot = $row ["Total"];
								$row ["Total"];
							}
						}
						
						$sql2 = "SELECT `Shop`, SUM(`Price`) AS Total FROM orderlist WHERE ShopId = 2";
						$result = $conn->query ( $sql2 );
						if ($result->num_rows > 0) {
							// output data of each row
							while ( $row = $result->fetch_assoc () ) {
								$tot2 = $row ["Total"];
								$row ["Total"];
							}
						}
						
						$sql3 = "SELECT `Shop`, SUM(`Price`) AS Total FROM orderlist WHERE ShopId = 3";
						$result = $conn->query ( $sql3 );
						if ($result->num_rows > 0) {
							// output data of each row
							while ( $row = $result->fetch_assoc () ) {
								$tot3 = $row ["Total"];
								$row ["Total"];
							}
						}
						
						$sql4 = "SELECT `Shop`, SUM(`Price`) AS Total FROM orderlist WHERE ShopId = 4";
						$result = $conn->query ( $sql4 );
						if ($result->num_rows > 0) {
							// output data of each row
							while ( $row = $result->fetch_assoc () ) {
								$tot4 = $row ["Total"];
								$row ["Total"];
							}
						}
						
						$sql5 = "SELECT `Shop`, SUM(`Price`) AS Total FROM orderlist WHERE ShopId = 5";
						$result = $conn->query ( $sql5 );
						if ($result->num_rows > 0) {
							// output data of each row
							while ( $row = $result->fetch_assoc () ) {
								$tot5 = $row ["Total"];
								$row ["Total"];
							}
						}
						$conn->close ();
						?>
    		<script>
            var freqData=[
            {State:'韓鶴亭',freq:{low:<?php echo $tot?>, mid:0, high:0}}
            ,{State:'GUGU廚房',freq:{low:<?php echo $tot2?>, mid:0, high:0}}
            ,{State:'鮮五丼',freq:{low:<?php echo $tot3?>, mid:0, high:0}}
            ,{State:'三商巧福',freq:{low:<?php echo $tot4?>, mid:0, high:0}}
            ,{State:'翰林茶館',freq:{low:<?php echo $tot5?>, mid:0, high:0}}
            ];
            dashboard('#dashboard',freqData);
            </script>
		</div>
	</div>
</body>
</html>
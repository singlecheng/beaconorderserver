<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "bryan_beaconorder";
$conn = new mysqli ( $servername, $username, $password, $dbname );
mysqli_set_charset ( $conn, "utf8" );

if ($_SESSION ['username'] == "data" or $_SESSION ['username'] == "admin") {
	?>
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - Data Analysis</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>
	<div class="container">
		<div class="jumbotron">
			<h1>Data Analysis</h1>
			<a href="/BeaconOrderServer/index.php/ERP/ShopReport"
				class="btn btn-info btn-lg"><span class="glyphicon glyphicon-search"></span>店家業績</a>
			<a href="/BeaconOrderServer/index.php/ERP/BeaconReport"
				class="btn btn-info btn-lg"><span class="glyphicon glyphicon-search"></span>Beacon使用程度</a>

			<hr>
			<?php
	$sql = "SELECT SUM(`Price`) AS Total FROM orderlist";
	$result = $conn->query ( $sql );
	if ($result->num_rows > 0) {
		?>
						<table class="table table-striped">
				<thead>
					<tr>
						<th>Total Shop Revenue</th>
					</tr>
				</thead>
				<tbody><?php
		// output data of each row
		while ( $row = $result->fetch_assoc () ) {
			echo "<tr><td>" . $row ["Total"] . "</td></tr>";
		}
		?></tbody>
			</table><?php
	} else {
		echo "0 results";
	}
	?>
	</div>
	</div>
</body>
</html><?php
} else {
	// send log
	$id = $_SESSION ['username'];
	$sql4 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject Form Data', CURRENT_TIMESTAMP)";
	$conn->query ( $sql4 ) === TRUE;
	
	echo 'Access Denied';
	echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php/Account/index>';
}
?>
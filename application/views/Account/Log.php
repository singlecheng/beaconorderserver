<!-- session check -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "bryan_beaconorder";
$conn = new mysqli ( $servername, $username, $password, $dbname );
mysqli_set_charset ( $conn, "utf8" );

if ($_SESSION ['username'] == "data" or $_SESSION ['username'] == "admin") {
	?>
<!-- page load -->
<!DOCTYPE html>
<html>
<head>
<title>Account - Log</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Log</h1>
			<p></p>
			<a href="/BeaconOrderServer/index.php/Account/create"
				class="btn btn-info btn-lg"><span class="button"></span>Create
				Account</a> <a href="/BeaconOrderServer/index.php/Account/log"
				class="btn btn-info btn-lg"><span class="button"></span>Account Log</a>
		</div>
		<a href="/BeaconOrderServer/index.php/Account/view">Back to list</a>
		<div>
			<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "bryan_beaconorder";
	
	// Create connection
	$conn = new mysqli ( $servername, $username, $password, $dbname );
	// 資料回傳編碼設UTF8
	mysqli_set_charset ( $conn, "utf8" );
	
	// Check connection
	if ($conn->connect_error) {
		die ( "Connection failed: " . $conn->connect_error );
	}
	
	$sql = "SELECT * FROM `accountlog` ORDER BY `accountlog`.`time` DESC LIMIT 50";
	$result = $conn->query ( $sql );
	
	if ($result->num_rows > 0) {
		?><table class="table table-striped">
				<form name="user" method="POST" action="">

					<thead>
						<tr>
							<th>ID</th>
							<th><span class="glyphicon glyphicon-tag"></span> Account Name</th>
							<th><span class="glyphicon glyphicon-import"></span> Action</th>
							<th><span class="glyphicon glyphicon-calendar"></span> Time</th>
						</tr>
					</thead>
					<tbody>
		<?php
		// output data of each row
		while ( $row = $result->fetch_assoc () ) {
			// 定義row出來的ID數值
			if ($row ["action"] == "Login") {
				echo "<tr class='info'><td>" . $row ["Id"] . "</td><td>" . $row ["username"] . "</td><td>" . $row ["action"] . "</td><td>" . $row ["time"] . "</td></tr>";
			} elseif ($row ["action"] == "Logout") {
				echo "<tr class='success'><td>" . $row ["Id"] . "</td><td>" . $row ["username"] . "</td><td>" . $row ["action"] . "</td><td>" . $row ["time"] . "</td></tr>";
			} elseif ($row ["action"] == "UpdateGroup" or $row ["action"] == "UpdateDetail") {
				echo "<tr class='warning'><td>" . $row ["Id"] . "</td><td>" . $row ["username"] . "</td><td>" . $row ["action"] . "</td><td>" . $row ["time"] . "</td></tr>";
			} else {
				echo "<tr class='danger'><td>" . $row ["Id"] . "</td><td>" . $row ["username"] . "</td><td>" . $row ["action"] . "</td><td>" . $row ["time"] . "</td></tr>";
			}
		}
		?></tbody>
				</form>
			</table><?php
	} else {
		echo "0 results";
	}
	$conn->close ();
	?>
				</div>
	</div>
</body>
</html><?php
} else {
	echo '您無權限觀看此頁面!';
	echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php/Account/index>';
}
?>
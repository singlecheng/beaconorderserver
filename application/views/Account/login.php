<!DOCTYPE html>
<html lang="en">
<head>
<title>Beacon Order Server - Account Login</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="/BeaconOrderServer/index.php">Beacon
					Order Server</a>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="jumbotron">
			<h1>
				<b>Beacon Order Server</b>
			</h1>

		</div>
	</div>
	<div class="container">
		<h2>Server Login</h2>
		<form role="form" method="post"
			action="/BeaconOrderServer/index.php/Account/connect">
			<div class="form-group">
				<label for="email">Account:</label> <input type="text"
					class="form-control" name="id" id="id" value=""
					placeholder="Enter username">
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label> <input type="password"
					class="form-control" name="pw" id="pw" value=""
					placeholder="Enter password">
			</div>
			<div class="checkbox">
				<label><input type="checkbox"> Remember me</label>
			</div>
			<button type="submit" class="btn btn-success">登入</button>
		</form>
	</div>

	<hr>
	<footer class="container-fluid bg-4 text-center">
		<p>
			Power By : <a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="http://d3js.org/">D3</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://developer.apple.com/swift/">Swift</a> | <a
				href="https://developer.apple.com/xcode/">Xcode</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		
		
		<p class="footer_copyright">
			© 2016 <a href="mailto:singleqiang@gmail.com">Single Studio</a>. All
			Rights Reserved.
		</p>
	</footer>
</body>
</html>
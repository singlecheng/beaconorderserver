﻿<!-- page load -->
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - CRM</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Customer Service</h1>
			<p></p>
		</div>
		<div>
			<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "bryan_beaconorder";
	
	// Create connection
	$conn = new mysqli ( $servername, $username, $password, $dbname );
	mysqli_set_charset ( $conn, "utf8" );
	// Check connection
	if ($conn->connect_error) {
		die ( "Connection failed: " . $conn->connect_error );
	}
	
	$sql = "SELECT * FROM `androidusers`";
	$result = $conn->query ( $sql );
	
	if ($result->num_rows > 0) {
		?><table class="table table-striped">
				<form name="user" method="POST"
					action="/BeaconOrderServer/index.php/CRM/Edit">
					<thead>
						<tr>
							<th>ID</th>
							<th><span class="glyphicon glyphicon-tag"></span> First Name</th>
							<th>Last Name</th>
							<th><span class="glyphicon glyphicon-envelope"></span> E-mail</th>
							<th><span class="glyphicon glyphicon-edit"></span> Edit</th>
						</tr>
					</thead>
					<tbody><?php
		// output data of each row
		while ( $row = $result->fetch_assoc () ) {
			// 定義row出來的ID數值
			$num = $row ["uid"];
			
			echo "<tr><td>" . $row ["uid"] . "</td><td>" . $row ["firstname"] . "</td><td>" . $row ["lastname"] . "</td><td>" . $row ["email"] . "</td><td>" . "<input type='submit' name='submit' class='btn btn-warning' value=$num ></td></tr>";
		}
		?></tbody>
				</form>
			</table><?php
	} else {
		echo "0 results";
	}
	
	$conn->close ();
	?>
		
		
		</div>
	</div>
</body>
</html>
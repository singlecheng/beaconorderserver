<!DOCTYPE html>
<html>
<head>
<title>CRM - Send AD</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>AD Edit</h1>
			<p></p>

		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/CRM">Back to list</a>

			<?php
			$id = $_POST ["submit"];
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "bryan_beaconorder";
			
			// Create connection
			$conn = new mysqli ( $servername, $username, $password, $dbname );
			
			// 資料回傳編碼設UTF8
			mysqli_set_charset ( $conn, "utf8" );
			
			// Check connection
			if ($conn->connect_error) {
				die ( "Connection failed: " . $conn->connect_error );
			}
			
			$sql = "SELECT * FROM `androidusers` WHERE `uid` = $id";
			$result = $conn->query ( $sql );
			
			if ($result->num_rows > 0) {
				?><table class="table">
				<tr>
					<th>ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>E-mail</th>
				</tr>
		
		<?php
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$email = $row ['email'];
					echo "<tr><td>" . $row ["uid"] . "</td><td>" . $row ["firstname"] . "</td><td>" . $row ["lastname"] . "</td><td>" . $row ["email"] . "</td></tr>";
				}
				?></table><?php
			} else {
				echo "0 results";
			}
			
			$conn->close ();
			?>
			
			<hr>

			<form method="POST" action="/BeaconOrderServer/index.php/CRM/Send">

				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-envelope"></span> E-mail</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=email name=email type=text value=$email>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="email" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-pencil"></span> Context</label>
					<div class="col-md-10" ng-app="myApp" ng-controller="myCtrl">
						<input class="form-control text-box single-line" id="Context"
							name="Context" type=text
							value='{{firstName + " " + lastName + " " + lastName2}}' />
						<P>(context select)</P>
						<select class="form-control" ng-model="firstName">
							<option selected="selected" ng-model="firstName">周年慶</option>
							<option ng-model="firstName">限時大優惠</option>
							<option ng-model="firstName">店長生日</option>
							<option ng-model="firstName">聖誕節</option>
							<option ng-model="firstName">過年優惠</option>
						</select> <select class="form-control" ng-model="lastName">
							<option selected="selected" ng-model="lastName">全面9折</option>
							<option ng-model="lastName">全面8折</option>
							<option ng-model="lastName">全面7折</option>
							<option ng-model="lastName">全面6折</option>
							<option ng-model="lastName">全面5折</option>
						</select> </select> <select class="form-control"
							ng-model="lastName2">
							<option selected="selected" ng-model="lastName2">搶便宜要快</option>
							<option ng-model="lastName">優惠只到月底哦 !</option>
							<option ng-model="lastName">來店消費就送好禮</option>
							<option ng-model="lastName">還有更多優等你來看</option>
							<option ng-model="lastName">不要錯過</option>
						</select>
					</div>

					<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function($scope) {
                        $scope.firstName = "";
                        $scope.lastName = "";
                        $scope.lastName2 = "";
                    });
                    </script>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-info"
							value="Send Ad">
					</div>
				</div>
			</form>

			<hr>
		</div>
	</div>
</body>
</html>
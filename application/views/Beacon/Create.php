<!DOCTYPE html>
<html>
<head>
<title>Create - Create</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Beacon Create</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Beacon">Back to list</a>
			<hr>

			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Beacon/Create">

				<div class="form-group">
					<label class="control-label col-md-2">Major</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Major"
							name="Major" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Major" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Minor</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Minor"
							name="Minor" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Minor" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Name</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Name"
							name="Name" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Name"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Locate</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Locate"
							name="Locate" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Locate" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Note</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Note"
							name="Note" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Note"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-default"
							value="確定新增">
					</div>
				</div>
			</form>

		</div>
	</div>
</body>
</html>
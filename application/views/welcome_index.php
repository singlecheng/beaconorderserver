<!DOCTYPE html>
<html lang="en">
<head>
<title>Beacon Order Server</title>
<meta charset="utf-8">
<link href="/BeaconOrderServer/assets/image/favicon.ico"
	rel="SHORTCUT ICON">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

<!-- Carousel Plugin -->
<style>
.carousel-inner>.item>img, .carousel-inner>.item>a>img {
	margin: auto;
}

.carousel {
	width: 100%;
	margin: auto;
}

.carousel-control.right, .carousel-control.left {
	background-image: none;
	color: #1abc9c;
}

.carousel-indicators li {
	border-color: #1abc9c;
}

.carousel-indicators li.active {
	background-color: #1abc9c;
}
</style>

<!-- navi bar -->
<style>
.bg-1 {
	background-color: #1abc9c;
	color: #ffffff;
}

.bg-2 {
	background-color: #474e5d;
	color: #ffffff;
}

.bg-3 {
	background-color: #ffffff;
	color: #555555;
}

.navbar-nav  li a:hover {
	color: #1abc9c !important;
}

.jumbotron {
	background-color: #00bbff;
	color: #fff;
	padding: 1px 25px;
	font-family: Montserrat, sans-serif;
}
</style>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="jumbotron text-center">
		<h1>
			<b><img src="/BeaconOrderServer/assets/image/favicon.ico" width=10%>Beacon
				Order Server</b>
		</h1>

	</div>

	<div class="container text-center ">
		<div class="panel-heading">
			<h2>
				<b><span class="glyphicon glyphicon-list-alt"></span> Project
					Prototype</b>
			</h2>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h3>
					<p class="bg-danger">
						<span class="glyphicon glyphicon-picture"></span> Why ?
					</p>
				</h3>
				<img src="/BeaconOrderServer/assets/image/1000.jpg"
					class="img-responsive" alt="Cinque Terre" width=100%>
			</div>
			<div class="col-md-4">
				<h3>
					<p class="bg-info">
						<span class="glyphicon glyphicon-picture"></span> Do
					</p>
				</h3>
				<img src="/BeaconOrderServer/assets/image/1001.jpg"
					class="img-responsive" alt="Cinque Terre" width=100%>
			</div>
			<div class="col-md-4">
				<h3>
					<p class="bg-success">
						<span class="glyphicon glyphicon-picture"></span> So
					</p>
				</h3>
				<img src="/BeaconOrderServer/assets/image/1002.jpg"
					class="img-responsive" alt="Cinque Terre" width=100%>
			</div>

		</div>
	</div>

	<div class="container text-center panel-info">
		<div class="panel-heading">
			<h2>
				<b><span class="glyphicon glyphicon-camera"></span> Android & Server
					Screenshot</b>
			</h2>
		</div>
		<div id="myCarousel" class="carousel slide row" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
				<li data-target="#myCarousel" data-slide-to="4"></li>
				<li data-target="#myCarousel" data-slide-to="5"></li>
				<li data-target="#myCarousel" data-slide-to="6"></li>
				<li data-target="#myCarousel" data-slide-to="7"></li>
				<li data-target="#myCarousel" data-slide-to="8"></li>
				<li data-target="#myCarousel" data-slide-to="9"></li>
				<li data-target="#myCarousel" data-slide-to="10"></li>
				<li data-target="#myCarousel" data-slide-to="11"></li>
				
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="/BeaconOrderServer/assets/image/a1.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>App Index</b></h3></div>
						
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a2.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Shop Menu</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a3.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Menu List</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a4.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b></b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a5.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Promotion</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a6.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Promotion</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a7.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Order via Promotion</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a8.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Order Confirm</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a9.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Orde Submit</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a10.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Order Complete</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a11.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>My Oeder</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a12.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Top 10</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a13.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Order List</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a14.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Order Edit</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a15.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Report</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a16.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b></b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a17.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Account Log</b></h3></div>
				</div>
				<div class="item">
					<img src="/BeaconOrderServer/assets/image/a18.jpg" width="460"
						height="345">
					<div class="carousel-caption"><h3><b>Promotion Edit</b></h3></div>
				</div>
				
			</div>
			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button"
				data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
				aria-hidden="true"></span> <span class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#myCarousel"
				role="button" data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>


	<div class="container panel-warning">
		<div class="panel-heading">
			<h2>
				<b>Team</b>
			</h2>
		</div>

		<div class="row">
			<div class="col-md-3">
				<h3>
					<b><p class="bg-warning text-center">
							<span class="glyphicon glyphicon-user"></span> 洪梓翔
						</p></b>
				</h3>
				<img src="/BeaconOrderServer/assets/image/24.jpg"
					class="img-responsive" alt="Cinque Terre" width=100%>
				<p>
					<b><span class="glyphicon glyphicon-ok"></span> Android App Develop<br>
						<span class="glyphicon glyphicon-ok"></span> Project Prototype
						Design<br> </b>
				</p>
			</div>
			<div class="col-md-3">
				<h3>
					<b><p class="bg-warning text-center">
							<span class="glyphicon glyphicon-user"></span> 王昭程
						</p></b>
				</h3>
				<img src="/BeaconOrderServer/assets/image/56.jpg"
					class="img-responsive" alt="Cinque Terre" width=100%>
				<p>
					<b><span class="glyphicon glyphicon-ok"></span> Web Designer<br>
						<span class="glyphicon glyphicon-ok"></span> Back-End Develop<br> </b>
				</p>
			</div>
			<div class="col-md-3">
				<h3>
					<b><p class="bg-warning text-center">
							<span class="glyphicon glyphicon-user"></span> 蔡一謙
						</p></b>
				</h3>
				<img src="/BeaconOrderServer/assets/image/26.jpg"
					class="img-responsive" alt="Cinque Terre" width=100%>
				<p>
					<b><span class="glyphicon glyphicon-ok"></span> Android App UI
						Design<br> <span class="glyphicon glyphicon-ok"></span> Project
						Analysis<br> </b>
				</p>
			</div>
			<div class="col-md-3">
				<h3>
					<b><p class="bg-warning text-center">
							<span class="glyphicon glyphicon-user"></span> 陳暐婷
						</p></b>
				</h3>
				<img src="/BeaconOrderServer/assets/image/21.jpg"
					class="img-responsive" alt="Cinque Terre" width=100%>
				<p>
					<b><span class="glyphicon glyphicon-ok"></span> Report Archive<br>
						<span class="glyphicon glyphicon-ok"></span> Material Collect<br>
					</b>
				</p>
			</div>
		</div>
	</div>

	<hr>
	<footer class="container-fluid bg-4 text-center">
		<p>
			Power By : <a href="https://www.android.com/">Android</a> | <a
				href="https://angularjs.org/">AngularJS</a> | <a
				href="https://bitbucket.org/">Bitbucket</a> | <a
				href="http://getbootstrap.com/">Bootstrap</a> | <a
				href="http://codeigniter.org.tw/">CodeIgniter</a> | <a
				href="http://d3js.org/">D3</a> | <a
				href="https://eclipse.org/downloads/">Eclipse</a> | <a
				href="http://estimote.com/">Estimote</a> | <a
				href="https://github.com/">GitHub</a> | <a
				href="https://developer.apple.com/ibeacon/">iBeacon</a> | <a
				href="https://jquery.com/">jQuery</a> | <a
				href="http://www.json.org/">JSON</a> | <a
				href="https://www.mysql.com/downloads/">MySQL</a> | <a
				href="http://php.net/downloads.php">PHP</a> | <a
				href="https://www.sourcetreeapp.com/">SourceTree</a> | <a
				href="https://www.apachefriends.org/zh_tw/index.html">Xampp</a> |
		</p>
		<p class="footer_copyright">
			© 2016 <a href="mailto:singleqiang@gmail.com">Single Studio</a>. All
			Rights Reserved.
		</p>
	</footer>
</body>
</html>
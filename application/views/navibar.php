<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/BeaconOrderServer/index.php">Beacon
				Order Server</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href="/BeaconOrderServer/index.php"><span
						class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="/BeaconOrderServer/index.php/Order"><span
						class="glyphicon glyphicon-shopping-cart"></span> Order</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"><span
						class="glyphicon glyphicon-th-list"></span> Shop<span
						class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/BeaconOrderServer/index.php/Shop">Shop</a></li>
						<li><a href="/BeaconOrderServer/index.php/Menu">Menu</a></li>
					</ul></li>
				<li><a href="/BeaconOrderServer/index.php/Promotion"><span
						class="glyphicon glyphicon-tags"></span> Promotion</a></li>
				<li><a href="/BeaconOrderServer/index.php/Beacon"><span
						class="glyphicon glyphicon-bitcoin"></span> Beacon</a></li>
				<li><a href="/BeaconOrderServer/index.php/Message"><span
						class="glyphicon glyphicon-cloud-upload"></span> Msg Board</a></li>
				<li><a href="/BeaconOrderServer/index.php/ERP"><span
						class="glyphicon glyphicon-stats"></span> Data Analysis</a></li>
				<li><a href="/BeaconOrderServer/index.php/CRM"><span
						class="glyphicon glyphicon-duplicate"></span> CRM</a></li>
				<li><a href="/BeaconOrderServer/index.php/Account/view"><span
						class="glyphicon glyphicon-cog"></span> Account Manage</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
					<!-- session load -->
					<?php
					if (isset($_SESSION ['username'])) {
						?><li><a href=""><span class="glyphicon glyphicon-user"></span>User : <?php echo $_SESSION['username'];?></a></li>
				<li><a href="/BeaconOrderServer/index.php/Account/logout"><span
						class="glyphicon glyphicon-log-in"></span> Logout</a></li><?php
					} 

					else {
						?><li><a href="/BeaconOrderServer/index.php/Account/"><span
						class="glyphicon glyphicon-log-in"></span> Login</a></li><?php
					}
					;
					?>
				</ul>
		</div>
	</div>
</nav>
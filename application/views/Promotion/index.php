﻿<!-- session check -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "bryan_beaconorder";
$conn = new mysqli ( $servername, $username, $password, $dbname );
mysqli_set_charset ( $conn, "utf8" );

if ($_SESSION ['username'] == "pro" or $_SESSION ['username'] == "admin") {
	?>
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - Promotion</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Promotion List</h1>
			<p></p>
			<a href="/BeaconOrderServer/index.php/Promotion/Create"
				class="btn btn-primary btn-lg"><span
				class="glyphicon glyphicon-plus-sign"></span> Create</a>
		</div>
		<div>
			<?php
	$sql = "SELECT * FROM `promotions` WHERE 1";
	$result = $conn->query ( $sql );
	
	if ($result->num_rows > 0) {
		?><table class="table table-striped">
				<form name="user" method="POST"
					action="/BeaconOrderServer/index.php/Promotion/Edit">

					<thead>
						<tr>
							<th>ID</th>
							<th><span class="glyphicon glyphicon-tag"></span> Name</th>
							<th><span class="glyphicon glyphicon-pencil"></span> Context</th>
							<th><span class="glyphicon glyphicon-bitcoin"></span> BeaconId</th>
							<!-- <th>Shop</th> -->
							<th>SalePrice</th>
							<th>ShopId</th>
							<th><span class="glyphicon glyphicon-time"></span> TIME</th>
							<th><span class="glyphicon glyphicon-edit"></span> Edit</th>
						</tr>
					</thead>
					<tbody>
			<?php
		// output data of each row
		while ( $row = $result->fetch_assoc () ) {
			// define row["ID"]
			$num = $row ["Id"];
			// define row["BeaconId"]
			$num2 = $row ["BeaconId"];
			
			echo "<tr><td>" . $row ["Id"] . "</td><td>" . $row ["Name"] . "</td><td>" . $row ["Context"] . "</td><td>" . "<input type='submit' name='submit' class='btn btn-info' value=$num2 ></td><td>" . $row ["SalePrice"] . "</td><td>" . $row ["ShopId"] . "</td><td>" . $row ["TIME"] . "</td><td>" . "<input type='submit' id='id' name='submit' class='btn btn-warning' value=$num >";
		}
		?></tbody>
				</form>
			</table><?php
	} else {
		echo "0 results";
	}
	$conn->close ();
	?>
				</div>
	</div>

</body>
</html><?php
} else {
	//send log
	$id = $_SESSION ['username'];
	$sql4 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject Form Promotion', CURRENT_TIMESTAMP)";
	$conn->query ( $sql4 ) === TRUE;
	
	echo 'Access Denied';
	echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php/Account/index>';
}
?>
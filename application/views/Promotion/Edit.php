<!DOCTYPE html>
<html>
<head>
<title>Promotion - Edit</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Promotion Edit</h1>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Promotion">Back to list</a>

			<?php
			$id = $_POST ["submit"];
			$BeaconId = $_POST ["submit"];
			
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "bryan_beaconorder";
			
			// Create connection
			$conn = new mysqli ( $servername, $username, $password, $dbname );
			
			// 資料回傳編碼設UTF8
			mysqli_set_charset ( $conn, "utf8" );
			
			// Check connection
			if ($conn->connect_error) {
				die ( "Connection failed: " . $conn->connect_error );
			}
			
			$sql = "SELECT * FROM `promotions` WHERE Id = $id";
			$result = $conn->query ( $sql );
			
			if ($result->num_rows > 0) {
				?><table class="table">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Context</th>
					<th>BeaconId</th>
					<th>SalePrice</th>
					<th>ShopId</th>
					<th>TIME</th>
				</tr>
		<?php
				// output data of each row
				while ( $row = $result->fetch_assoc () ) {
					$num = $row ["Id"];
					$num2 = $row ["BeaconId"];
					echo "<tr><td>" . $row ["Id"] . "</td><td>" . $row ["Name"] . "</td><td>" . $row ["Context"] . "</td><td>" . $row ["BeaconId"] . "</td><td>" . $row ["SalePrice"] . "</td><td>" . $row ["ShopId"] . "</td><td>" . $row ["TIME"] . "</td></tr>";
				}
				?>
			</table><?php
			} else {
				echo "0 results";
			}
			
			$conn->close ();
			?>
			
			<hr>
			<form method="POST"
				action="/BeaconOrderServer/index.php/Promotion/Delete">

				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-danger"
							value="Delete">
					</div>
				</div>
			</form>

			<hr>

			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Promotion/Update">
				<div class="form-group">
					<label class="control-label col-md-2">Id</label>
					<div class="col-md-10">
	                <?php echo "<input class=form-control text-box single-line id=Id name=Id type=text value=$id>"?>
	                <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2" for=""><span
						class="glyphicon glyphicon-tag"></span> Name</label>
					<div class="col-md-10">
						<select class="form-control" id="Name" name="Name">
							<option selected="selected" value="韓鶴亭">韓鶴亭</option>
							<option value="GUGU廚房">GUGU廚房</option>
							<option value="鮮五丼">鮮五丼</option>
							<option value="三商巧福">三商巧福</option>
							<option value="	翰林茶館">翰林茶館</option>
						</select> <span class="field-validation-valid text-danger"
							data-valmsg-for="Name" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-pencil"></span> Context</label>
					<div class="col-md-10" ng-app="myApp" ng-controller="myCtrl">
						<input class="form-control text-box single-line" id="Context"
							name="Context" type=text value='{{firstName + " " + lastName}}' />
						<P>(context select)</P>
						<select class="form-control" ng-model="firstName">
							<option selected="selected" ng-model="firstName">周年慶</option>
							<option ng-model="firstName">限時大優惠</option>
							<option ng-model="firstName">店長生日</option>
							<option ng-model="firstName">聖誕節</option>
							<option ng-model="firstName">過年優惠</option>
						</select> <select class="form-control" ng-model="lastName">
							<option selected="selected" ng-model="lastName">全面9折</option>
							<option ng-model="lastName">全面8折</option>
							<option ng-model="lastName">全面7折</option>
							<option ng-model="lastName">全面6折</option>
							<option ng-model="lastName">全面5折</option>
						</select>
					</div>

					<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function($scope) {
                        $scope.firstName = "";
                        $scope.lastName = "";
                    });
                    </script>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2" for=""><span
						class="glyphicon glyphicon-bitcoin"></span> Beacon Id</label>
					<div class="col-md-10">
						<select class="form-control" id="BeaconId" name="BeaconId">
							<option value="<?php echo $num2?>"><?php echo $num2?></option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
						</select> <span class="field-validation-valid text-danger"
							data-valmsg-for="BeaconId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<!-- <div class="form-group">
					<label class="control-label col-md-2" for="">Shop</label>
					<div class="col-md-10">
						<select class="form-control" id="Shop" name="Shop">
							<option selected="selected" value="韓鶴亭">韓鶴亭</option>
							<option value="GUGU廚房">GUGU廚房</option>
							<option value="鮮五丼">鮮五丼</option>
							<option value="三商巧福">三商巧福</option>
							<option value="	翰林茶館">翰林茶館</option>
						</select><span class="field-validation-valid text-danger"
							data-valmsg-for="Shop" data-valmsg-replace="true"></span>
					</div>
				</div> -->


				<div class="form-group">
					<label class="control-label col-md-2">Discount</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="SalePrice"
							name="SalePrice" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="SalePrice" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2" for="">Shop Id</label>
					<div class="col-md-10">
						<select class="form-control" id="ShopId" name="ShopId">
							<option selected="selected" value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select> <span class="field-validation-valid text-danger"
							data-valmsg-for="ShopId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">TIME</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="TIME"
							name="TIME" type="text" value="00:01~23:59"><span
							class="field-validation-valid text-danger" data-valmsg-for="TIME"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-success"
							value="Edit Confirm">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
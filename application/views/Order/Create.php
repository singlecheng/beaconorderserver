<!DOCTYPE html>
<html>
<head>
<title>Order - Create</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Order Create</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Order">Back to list</a>
			<hr>

			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Order/Create">
				<div class="form-group">
					<label class="control-label col-md-2">Shop</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Shop"
							name="Shop" type="text" value=""> <span
							class="field-validation-valid text-danger" data-valmsg-for="Shop"
							data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Product</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Product"
							name="Product" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Product" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Count</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Count"
							name="Count" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Count" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Price</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Price"
							name="Price" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Price" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Purchase</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Purchase"
							name="Purchase" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Purchase" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">CustomerEmail</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line"
							id="ProCustomerEmailduct2" name="CustomerEmail" type="text"
							value=""> <span class="field-validation-valid text-danger"
							data-valmsg-for="CustomerEmail" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">BeaconId</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="BeaconId"
							name="BeaconId" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="BeaconId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">ProductId</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="ProductId"
							name="ProductId" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="ProductId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">ShopId</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="ShopId"
							name="ShopId" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="ShopId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">CustomerId</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="CustomerId"
							name="CustomerId" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="CustomerId" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2">Purchase</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Purchase"
							name="Purchase" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Purchase" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-success"
							value="Confirm">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
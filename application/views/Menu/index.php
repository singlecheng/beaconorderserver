<!-- session check -->
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "bryan_beaconorder";
$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conn, "utf8");

if ($_SESSION['username'] == "menu" or $_SESSION['username'] == "admin") {
    ?>
<!-- page load -->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Beacon Order Server - Menu</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Menu List</h1>
			<p></p>

		</div>

		<div>
		
		<?php
    $sql = "SELECT `Id`, `Name`, `type`, `price`, `Shop`, `ShopId` FROM `menuproduct` WHERE 1";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        ?><table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>type</th>
						<th>price</th>
						<th>Shop</th>
						<th>ShopId</th>
					</tr>
				</thead>
				<tbody><?php
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $row["Id"] . "</td><td>" . $row["Name"] .
                     "</td><td>" . $row["type"] . "</td><td>" . $row["price"] .
                     "</td><td>" . $row["Shop"] . "</td><td>" . $row["ShopId"];
        }
        ?></tbody>
			</table><?php
    } else {
        echo "0 results";
    }
    $conn->close();
    ?>
		
		
		</div>
	</div>
</body>
</html><?php
} else {
    // send log
    $id = $_SESSION['username'];
    $sql4 = "INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES (NULL, '$id', 'Reject Form Menu', CURRENT_TIMESTAMP)";
    $conn->query($sql4) === TRUE;
    echo 'Access Denied';
    echo '<meta http-equiv=REFRESH CONTENT=2;url=/BeaconOrderServer/index.php/Account/index>';
}
?>
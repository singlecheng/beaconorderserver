<!DOCTYPE html>
<html>
<head>
<title>Message - Create</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
	href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- load navibar -->
<?php $this->load->view('navibar');?>

	<div class="container">
		<div class="jumbotron">
			<h1>Write Message</h1>
			<p></p>
		</div>
		<div class="form-horizontal">
			<a href="/BeaconOrderServer/index.php/Message">Back to list</a>
			<hr>
			<form name="user" method="POST"
				action="/BeaconOrderServer/index.php/Message/Create">
				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-envelope"></span> Email</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Email"
							name="Email" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Email" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-bookmark"></span> Subject</label>
					<div class="col-md-10">
						<input class="form-control text-box single-line" id="Subject"
							name="Subject" type="text" value=""> <span
							class="field-validation-valid text-danger"
							data-valmsg-for="Subject" data-valmsg-replace="true"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-2"><span
						class="glyphicon glyphicon-pencil"></span> Context<br> </label>
					<div class="col-md-10">
						<textarea class="form-control" rows="5" id="Context"
							name="Context" value=''></textarea>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<input type="submit" name="Submit" class="btn btn-default"
							value="Submit Message">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
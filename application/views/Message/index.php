﻿<!-- page load -->
<!DOCTYPE html>
<html>
<head>
<title>Beacon Order Server - Message</title>
<meta charset="utf-8">
<!--loag bootstrap css  -->
<meta name="viewport" content="width=device-width, initial-scal=1">
<link rel="stylesheet"
href="/BeaconOrderServer/assets/css/bootstrap.min.css">
<script src="/BeaconOrderServer/assets/js/jquery-1.11.3.min.js"></script>
<script src="/BeaconOrderServer/assets/js/bootstrap.min.js"></script>
</head>
<body>
<!-- load navibar -->
<?php $this->load->view('navibar');?>

<div class="container">
<div class="jumbotron">
<h1>Message Board List</h1>
<p></p>
<a href="/BeaconOrderServer/index.php/Message/Create" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus-sign"></span> Write Message</a>
</div>
<div>
	<table class="table table-striped">
		<thead>
			<tr>
			<th>ID</th>
			<th>Reply</th>
			<th>Subject</th>
			<th>Context</th>
			<th>E-Mail</th>
			<th>TIME</th>
			<th>Reply</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($boardlist as $store)
			{
				$num = $store->Id;
				if($store->Reply == "0")
				{
					echo "<tr class='danger'><td>" . $store->Id . "</td><td>" . $store->Reply . "</td><td>" . $store->Subject . "</td><td>" . $store->Context . "</td><td>" . $store->Email . "</td><td>" . $store->TIME . "</td><td>" . "<input type='submit' name='submit' class='btn btn-warning' value=$num ></td></tr>";  
				}else {
					echo "<tr class='success'><td>" . $store->Id . "</td><td>" . $store->Reply . "</td><td>" . $store->Subject . "</td><td>" . $store->Context . "</td><td>" . $store->Email . "</td><td>" . $store->TIME . "</td><td>" . "<input type='submit' name='submit' class='btn btn-warning' value=$num ></td></tr>";  
				}
			}
			?>
		</tbody>
	</table>
</div>
</body>
</html>
<?php
class MessageModel extends CI_Model {

        // public $title;
        // public $content;
        // public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function GetMessageBoardList()
        {
            $sql = $this->db->query('SELECT * FROM `msgboard` ORDER BY `msgboard`.`TIME` DESC');
            return $sql->result();
        }
}
?>
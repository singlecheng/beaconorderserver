<?php
class OrderModel extends CI_Model {

        // public $title;
        // public $content;
        // public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function GetOrderGroupList()
        {
                $sql = $this->db->query('SELECT * FROM `ordergroup` ORDER BY `ordergroup`.`CeateTime` DESC');
                return $sql->result();
        }

        public function GetOrderGroupCount()
        {
                $sql = $this->db->query('SELECT ( SELECT COUNT(*) FROM ordergroup ) AS count1, ( SELECT COUNT(*) FROM orderlist ) AS count2 FROM dual');
                return $sql->result();
        }
}
?>
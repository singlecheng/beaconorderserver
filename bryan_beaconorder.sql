-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2015-12-22 22:05:38
-- 伺服器版本: 10.1.9-MariaDB
-- PHP 版本： 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `bryan_beaconorder`
--

-- --------------------------------------------------------

--
-- 資料表結構 `accountlog`
--

CREATE TABLE `accountlog` (
  `Id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `accountlog`
--

INSERT INTO `accountlog` (`Id`, `username`, `action`, `time`) VALUES
(1, 'admin', 'Login', '2015-12-10 03:31:27'),
(2, 'admin', 'Logout', '2015-12-10 03:35:34'),
(3, 'admin', 'Login', '2015-12-10 03:37:43'),
(4, 'admin', 'Logout', '2015-12-10 03:37:45'),
(5, 'shop', 'Login', '2015-12-10 03:37:51'),
(6, 'shop', 'Logout', '2015-12-10 03:37:54'),
(7, 'bea', 'Login', '2015-12-10 03:38:03'),
(8, 'order', 'Login', '2015-12-10 03:44:39'),
(9, 'order', 'Reject', '2015-12-10 03:44:48'),
(10, 'shop', 'Login', '2015-12-10 03:45:00'),
(11, 'shop', 'Reject', '2015-12-10 03:45:03'),
(12, 'admin', 'Login', '2015-12-10 03:45:18'),
(13, '', 'Reject', '2015-12-10 04:32:30'),
(14, 'admin', 'Login', '2015-12-10 04:32:48'),
(15, 'admin', 'Login', '2015-12-10 04:38:59'),
(16, '', 'Reject', '2015-12-10 07:36:01'),
(17, 'admin', 'Login', '2015-12-10 07:36:05'),
(18, 'admin', 'UpdateGroup', '2015-12-10 07:37:17'),
(19, 'admin', 'Login', '2015-12-10 09:00:19'),
(20, '', 'Reject', '2015-12-10 09:58:48'),
(21, 'admin', 'Login', '2015-12-10 11:15:40'),
(22, 'admin', 'Login', '2015-12-10 11:53:29'),
(23, '', 'Reject', '2015-12-10 12:17:36'),
(24, 'admin', 'Login', '2015-12-10 12:17:52'),
(25, 'admin', 'Login', '2015-12-10 12:21:26'),
(26, 'admin', 'UpdateDetail', '2015-12-10 12:22:56'),
(27, 'admin', 'UpdateGroup', '2015-12-10 12:23:10'),
(28, 'admin', 'Login', '2015-12-10 12:25:05'),
(29, '', 'Reject', '2015-12-10 12:40:00'),
(30, 'admin', 'Login', '2015-12-10 12:40:21'),
(31, 'admin', 'UpdateGroup', '2015-12-10 12:50:53'),
(32, 'admin', 'UpdateDetail', '2015-12-10 12:52:04'),
(33, 'admin', 'UpdateGroup', '2015-12-10 13:00:28'),
(34, 'admin', 'UpdateDetail', '2015-12-10 13:00:37'),
(35, 'admin', 'UpdateDetail', '2015-12-10 13:00:45'),
(36, 'admin', 'UpdateDetail', '2015-12-10 13:00:55'),
(37, 'admin', 'UpdateGroup', '2015-12-10 13:01:03'),
(38, 'admin', 'UpdateGroup', '2015-12-10 13:01:12'),
(39, 'admin', 'UpdateGroup', '2015-12-10 13:01:19'),
(40, 'admin', 'UpdateGroup', '2015-12-10 13:01:30'),
(41, 'admin', 'Login', '2015-12-10 14:08:48'),
(42, '', 'Reject', '2015-12-11 15:11:24'),
(43, 'admin', 'Login', '2015-12-11 15:11:29'),
(44, '', 'Reject', '2015-12-12 09:58:46'),
(45, '', 'Reject', '2015-12-12 10:04:13'),
(46, '', 'Reject', '2015-12-12 12:39:28'),
(47, 'admin', 'Login', '2015-12-12 12:39:36'),
(48, 'admin', 'Logout', '2015-12-12 15:17:30'),
(49, 'admin', 'Login', '2015-12-12 15:17:36'),
(50, 'admin', 'Logout', '2015-12-12 15:17:40'),
(51, '', 'Reject', '2015-12-12 15:17:42'),
(52, '', 'Reject', '2015-12-12 15:17:57'),
(53, '', 'Reject', '2015-12-12 15:17:59'),
(54, '', 'Reject', '2015-12-12 15:18:00'),
(55, '', 'Reject', '2015-12-12 15:19:51'),
(56, '', 'Reject', '2015-12-12 15:19:57'),
(57, '', 'Reject', '2015-12-12 15:20:02'),
(58, 'admin', 'Login', '2015-12-12 15:20:07'),
(59, 'admin', 'Logout', '2015-12-12 15:23:57'),
(60, '', 'Reject Form Order', '2015-12-12 15:24:00'),
(61, '', 'Reject Form Shop', '2015-12-12 15:24:04'),
(62, '', 'Reject Form Promotion', '2015-12-12 15:24:08'),
(63, '', 'Reject Form Data', '2015-12-12 15:24:11'),
(64, '', 'Reject From Account', '2015-12-12 15:24:15'),
(65, 'admin', 'Login', '2015-12-12 15:24:21'),
(66, 'admin', 'Logout', '2015-12-12 15:24:31'),
(67, '', 'Reject From Account', '2015-12-12 15:24:34'),
(68, 'admin', 'Login', '2015-12-12 15:24:38'),
(69, 'admin', 'Logout', '2015-12-12 15:25:19'),
(70, '', 'Reject Form Order', '2015-12-12 15:35:57'),
(71, 'admin', 'Login', '2015-12-12 15:36:07'),
(72, '', 'Reject Form Order', '2015-12-12 15:52:44'),
(73, 'admin', 'Login', '2015-12-12 15:52:53'),
(74, '', 'Reject Form Beacon', '2015-12-12 16:54:03'),
(75, 'admin', 'Login', '2015-12-12 16:54:10'),
(76, 'admin', 'Logout', '2015-12-12 17:04:52'),
(77, 'shop', 'Login', '2015-12-12 17:04:58'),
(78, 'shop', 'Reject Form Order', '2015-12-12 17:05:00'),
(79, 'admin', 'Login', '2015-12-12 17:05:05'),
(80, 'admin', 'Logout', '2015-12-12 17:06:21'),
(81, '', 'Reject From Account', '2015-12-12 17:09:04'),
(82, 'admin', 'Login', '2015-12-12 17:09:11'),
(83, '', 'Reject Form Order', '2015-12-12 17:46:12'),
(84, 'admin', 'Login', '2015-12-12 17:46:22'),
(85, '', 'Reject Form Promotion', '2015-12-12 18:38:56'),
(86, 'admin', 'Login', '2015-12-12 18:39:03'),
(87, '', 'Reject Form Order', '2015-12-12 21:58:35'),
(88, '', 'Reject Form Order', '2015-12-12 21:58:46'),
(89, 'admin', 'Login', '2015-12-12 21:58:51'),
(90, '', 'Reject Form Order', '2015-12-13 00:03:18'),
(91, 'admin', 'Login', '2015-12-13 00:03:29'),
(92, 'admin', 'Logout', '2015-12-13 00:03:38'),
(93, '0', 'Login Failed', '2015-12-13 00:09:13'),
(94, 'admin', 'Login', '2015-12-13 00:09:17'),
(95, 'admin', 'Logout', '2015-12-13 00:09:32'),
(96, 'admin', 'Login', '2015-12-13 00:09:41'),
(97, 'admin', 'Logout', '2015-12-13 00:09:52'),
(98, 'dasdasdsads', 'Login Failed', '2015-12-13 00:10:02'),
(99, 'admin', 'Login', '2015-12-13 00:10:07'),
(100, 'admin', 'Logout', '2015-12-13 00:19:15'),
(101, '', 'Reject Form Promotion', '2015-12-13 00:27:33'),
(102, 'admin', 'Login', '2015-12-13 00:27:38'),
(103, '', 'Reject Form Promotion', '2015-12-13 00:27:42'),
(104, 'admin', 'Login', '2015-12-13 00:27:46'),
(105, 'admin', 'UpdateGroup', '2015-12-13 00:30:52'),
(106, 'admin', 'UpdateGroup', '2015-12-13 00:45:09'),
(107, 'admin', 'UpdateGroup', '2015-12-13 00:47:33'),
(108, 'admin', 'UpdateGroup', '2015-12-13 00:47:41'),
(109, 'admin', 'UpdateGroup', '2015-12-13 01:17:48'),
(110, 'admin', 'UpdateGroup', '2015-12-13 01:17:59'),
(111, 'admin', 'UpdateGroup', '2015-12-13 01:24:26'),
(112, 'admin', 'UpdateGroup', '2015-12-13 01:24:57'),
(113, 'admin', 'UpdateGroup', '2015-12-13 01:25:25'),
(114, 'admin', 'UpdateGroup', '2015-12-13 01:26:50'),
(115, 'admin', 'UpdateGroup', '2015-12-13 01:27:11'),
(116, '', 'Reject Form Order', '2015-12-13 04:40:33'),
(117, '', 'Reject Form Order', '2015-12-13 04:40:36'),
(118, 'admin', 'Login', '2015-12-13 04:40:40'),
(119, 'admin', 'Login', '2015-12-13 04:40:44'),
(120, 'admin', 'Logout', '2015-12-13 10:27:23'),
(121, 'admin', 'Login', '2015-12-13 10:27:29'),
(122, 'admin', 'Login', '2015-12-13 12:08:35'),
(123, '', 'Reject Form Order', '2015-12-13 12:46:26'),
(124, 'admin', 'Login', '2015-12-13 12:47:03'),
(125, 'admin', 'UpdateGroup', '2015-12-13 12:48:49'),
(126, 'admin', 'UpdateGroup', '2015-12-13 12:49:09'),
(127, 'admin', 'Login', '2015-12-13 13:07:58'),
(128, 'admin', 'UpdateDetail', '2015-12-13 13:11:28'),
(129, 'admin', 'UpdateDetail', '2015-12-13 13:11:38'),
(130, 'admin', 'UpdateGroup', '2015-12-13 13:11:50'),
(131, '', 'Reject Form Order', '2015-12-13 13:14:53'),
(132, 'admin', 'Login', '2015-12-13 13:15:03'),
(133, 'admin', 'UpdateDetail', '2015-12-13 13:16:55'),
(134, 'admin', 'UpdateDetail', '2015-12-13 13:17:51'),
(135, 'admin', 'UpdateDetail', '2015-12-13 13:18:10'),
(136, 'admin', 'UpdateDetail', '2015-12-13 13:18:31'),
(137, 'admin', 'UpdateGroup', '2015-12-13 13:18:47'),
(138, 'admin', 'UpdateGroup', '2015-12-13 13:19:48'),
(139, 'admin', 'UpdateGroup', '2015-12-13 13:20:21'),
(140, 'admin', 'UpdateGroup', '2015-12-13 13:20:28'),
(141, 'admin', 'UpdateGroup', '2015-12-13 13:20:38'),
(142, '', 'Reject Form Order', '2015-12-13 13:27:05'),
(143, '', 'Reject Form Beacon', '2015-12-13 13:38:54'),
(144, 'admin', 'Login', '2015-12-13 13:39:08'),
(145, 'admin', 'UpdateDetail', '2015-12-13 13:59:26'),
(146, 'admin', 'UpdateDetail', '2015-12-13 13:59:53'),
(147, 'admin', 'UpdateGroup', '2015-12-13 14:00:00'),
(148, 'admin', 'UpdateDetail', '2015-12-13 14:00:29'),
(149, 'admin', 'UpdateDetail', '2015-12-13 14:00:37'),
(150, 'admin', 'UpdateGroup', '2015-12-13 14:00:43'),
(151, 'admin', 'UpdateDetail', '2015-12-13 14:01:00'),
(152, 'admin', 'UpdateGroup', '2015-12-13 14:01:06'),
(153, '', 'Reject Form Order', '2015-12-13 17:05:03'),
(154, '', 'Reject From Account', '2015-12-13 18:35:49'),
(155, 'admin', 'Login', '2015-12-13 18:35:57'),
(156, 'admin', 'Login', '2015-12-13 18:43:44'),
(157, '', 'Reject Form Order', '2015-12-13 19:08:58'),
(158, '', 'Reject Form Order', '2015-12-13 19:09:01'),
(159, '', 'Reject Form Order', '2015-12-13 20:20:52'),
(160, 'admin', 'Login', '2015-12-13 20:20:56'),
(161, 'admin', 'UpdateGroup', '2015-12-13 20:21:08'),
(162, 'admin', 'UpdateGroup', '2015-12-13 20:21:20'),
(163, 'admin', 'UpdateDetail', '2015-12-13 20:25:17'),
(164, 'admin', 'UpdateDetail', '2015-12-13 20:25:28'),
(165, 'admin', 'UpdateGroup', '2015-12-13 20:25:36'),
(166, 'admin', 'UpdateGroup', '2015-12-13 22:13:30'),
(167, 'admin', 'UpdateDetail', '2015-12-13 22:15:17'),
(168, 'admin', 'UpdateDetail', '2015-12-13 22:15:23'),
(169, 'admin', 'UpdateGroup', '2015-12-13 22:15:31'),
(170, 'admin', 'UpdateDetail', '2015-12-13 22:15:40'),
(171, 'admin', 'UpdateDetail', '2015-12-13 22:15:51'),
(172, 'admin', 'UpdateDetail', '2015-12-13 22:16:03'),
(173, 'admin', 'UpdateGroup', '2015-12-13 22:16:08'),
(174, 'admin', 'UpdateGroup', '2015-12-13 22:25:15'),
(175, 'admin', 'UpdateGroup', '2015-12-14 03:54:08'),
(176, 'admin', 'Login', '2015-12-14 08:33:14'),
(177, 'admin', 'UpdateGroup', '2015-12-14 09:20:24'),
(178, 'admin', 'UpdateDetail', '2015-12-14 09:20:37'),
(179, 'admin', 'UpdateDetail', '2015-12-14 09:20:42'),
(180, 'admin', 'UpdateDetail', '2015-12-14 09:20:47'),
(181, 'admin', 'UpdateGroup', '2015-12-14 09:20:52'),
(182, 'admin', 'UpdateDetail', '2015-12-14 09:21:28'),
(183, 'admin', 'UpdateGroup', '2015-12-14 09:21:32'),
(184, 'admin', 'UpdateGroup', '2015-12-14 09:21:36'),
(185, 'admin', 'UpdateDetail', '2015-12-14 09:21:49'),
(186, 'admin', 'UpdateDetail', '2015-12-14 09:21:55'),
(187, '', 'Reject Form Order', '2015-12-14 10:33:08'),
(188, 'admin', 'Login', '2015-12-14 10:33:14'),
(189, 'admin', 'UpdateGroup', '2015-12-14 10:39:20'),
(190, 'admin', 'UpdateDetail', '2015-12-14 10:39:30'),
(191, 'admin', 'UpdateDetail', '2015-12-14 10:39:36'),
(192, 'admin', 'UpdateGroup', '2015-12-14 10:39:50'),
(193, 'admin', 'UpdateGroup', '2015-12-14 10:40:30'),
(194, 'admin', 'UpdateDetail', '2015-12-14 10:43:01'),
(195, 'admin', 'UpdateDetail', '2015-12-14 10:43:08'),
(196, 'admin', 'UpdateDetail', '2015-12-14 10:43:14'),
(197, 'admin', 'UpdateGroup', '2015-12-14 10:43:25'),
(198, 'admin', 'UpdateGroup', '2015-12-14 10:43:38'),
(199, '', 'Reject Form Order', '2015-12-15 00:35:59'),
(200, 'admin', 'Login', '2015-12-15 00:36:05'),
(201, '', 'Reject Form Data', '2015-12-15 09:20:54'),
(202, 'admin', 'Login', '2015-12-15 09:20:59'),
(203, 'admin', 'Logout', '2015-12-15 09:42:09'),
(204, 'order', 'Login', '2015-12-15 09:42:17'),
(205, 'order', 'Logout', '2015-12-15 09:42:27'),
(206, '', 'Reject Form Shop', '2015-12-15 09:46:25'),
(207, 'admin', 'Login', '2015-12-15 09:46:31'),
(208, '', 'Reject Form Shop', '2015-12-15 11:18:55'),
(209, 'admin', 'Login', '2015-12-15 11:19:00'),
(210, '', 'Reject Form Order', '2015-12-15 12:57:55'),
(211, '', 'Reject Form Promotion', '2015-12-15 13:00:54'),
(212, 'admin', 'Login', '2015-12-15 13:01:29'),
(213, '', 'Reject Form Shop', '2015-12-15 23:44:23'),
(214, 'admin', 'Login', '2015-12-15 23:44:31'),
(215, 'admin', 'UpdateGroup', '2015-12-15 23:58:06'),
(216, 'admin', 'Logout', '2015-12-16 00:12:43'),
(217, 'admin', 'Login', '2015-12-16 00:21:58'),
(218, 'admin', 'UpdateGroup', '2015-12-16 00:43:45'),
(219, 'admin', 'UpdateGroup', '2015-12-16 00:43:56'),
(220, 'admin', 'UpdateGroup', '2015-12-16 00:44:00'),
(221, 'admin', 'UpdateGroup', '2015-12-16 00:44:22'),
(222, 'admin', 'UpdateGroup', '2015-12-16 00:45:12'),
(223, 'admin', 'UpdateGroup', '2015-12-16 00:48:33'),
(224, 'admin', 'UpdateGroup', '2015-12-16 00:48:53'),
(225, 'admin', 'UpdateGroup', '2015-12-16 00:49:16'),
(226, 'admin', 'UpdateGroup', '2015-12-16 00:49:25'),
(227, 'admin', 'UpdateGroup', '2015-12-16 00:49:56'),
(228, 'admin', 'UpdateGroup', '2015-12-16 00:52:36'),
(229, 'admin', 'UpdateGroup', '2015-12-16 00:54:52'),
(230, 'admin', 'UpdateGroup', '2015-12-16 00:55:57'),
(231, 'admin', 'UpdateGroup', '2015-12-16 00:56:52'),
(232, 'admin', 'UpdateGroup', '2015-12-16 00:59:15'),
(233, 'admin', 'UpdateGroup', '2015-12-16 00:59:41'),
(234, 'admin', 'UpdateGroup', '2015-12-16 01:00:47'),
(235, '', 'Reject Form Order', '2015-12-17 17:06:45'),
(236, 'admin', 'Login', '2015-12-17 17:06:49'),
(237, 'admin', 'UpdateGroup', '2015-12-17 17:07:04'),
(238, '', 'Reject Form Shop', '2015-12-17 17:22:35'),
(239, 'admin', 'Login', '2015-12-17 17:22:41'),
(240, 'admin', 'Login', '2015-12-17 22:04:56'),
(241, 'admin', 'UpdateGroup', '2015-12-17 22:09:33'),
(242, 'admin', 'UpdateGroup', '2015-12-17 22:09:45'),
(243, 'admin', 'UpdateGroup', '2015-12-17 22:18:12'),
(244, 'admin', 'UpdateGroup', '2015-12-17 22:18:20'),
(245, 'admin', 'UpdateGroup', '2015-12-17 22:35:47'),
(246, 'admin', 'UpdateGroup', '2015-12-17 22:35:55'),
(247, 'admin', 'UpdateGroup', '2015-12-18 04:24:07'),
(248, 'admin', 'UpdateGroup', '2015-12-18 04:24:20'),
(249, '', 'Reject Form Order', '2015-12-18 12:35:18'),
(250, 'admin', 'Login', '2015-12-18 12:35:22'),
(251, 'admin', 'UpdateDetail', '2015-12-18 12:35:34'),
(252, 'admin', 'UpdateGroup', '2015-12-18 12:35:42'),
(253, 'admin', 'UpdateGroup', '2015-12-18 12:35:48'),
(254, 'admin', 'UpdateGroup', '2015-12-18 16:03:21'),
(255, 'admin', 'UpdateGroup', '2015-12-18 16:03:25'),
(256, 'admin', 'UpdateGroup', '2015-12-18 18:16:37'),
(257, 'admin', 'UpdateGroup', '2015-12-18 18:16:43'),
(258, '', 'Reject Form Order', '2015-12-18 18:53:34'),
(259, '', 'Reject Form Data', '2015-12-18 19:16:40'),
(260, 'admin', 'Login', '2015-12-18 19:16:47'),
(261, 'admin', 'UpdateGroup', '2015-12-18 19:55:53'),
(262, 'admin', 'UpdateDetail', '2015-12-18 19:55:59'),
(263, 'admin', 'UpdateDetail', '2015-12-18 19:56:05'),
(264, 'admin', 'UpdateGroup', '2015-12-18 19:56:09'),
(265, 'admin', 'UpdateDetail', '2015-12-18 19:56:14'),
(266, 'admin', 'UpdateDetail', '2015-12-18 19:56:19'),
(267, 'admin', 'UpdateGroup', '2015-12-18 19:56:23'),
(268, 'admin', 'UpdateDetail', '2015-12-18 19:56:39'),
(269, 'admin', 'UpdateDetail', '2015-12-18 19:56:55'),
(270, 'admin', 'UpdateDetail', '2015-12-18 19:56:59'),
(271, 'admin', 'UpdateDetail', '2015-12-18 19:57:03'),
(272, 'admin', 'UpdateGroup', '2015-12-18 19:57:10'),
(273, 'admin', 'UpdateDetail', '2015-12-18 19:57:15'),
(274, 'admin', 'UpdateGroup', '2015-12-18 19:57:19'),
(275, 'admin', 'UpdateDetail', '2015-12-18 19:57:25'),
(276, 'admin', 'UpdateGroup', '2015-12-18 19:57:29'),
(277, 'admin', 'UpdateDetail', '2015-12-18 19:57:36'),
(278, 'admin', 'UpdateDetail', '2015-12-18 19:57:40'),
(279, 'admin', 'Login', '2015-12-18 20:55:11'),
(280, 'admin', 'UpdateGroup', '2015-12-18 21:20:27'),
(281, 'admin', 'UpdateGroup', '2015-12-18 21:20:34'),
(282, '', 'Reject Form Order', '2015-12-19 01:50:45'),
(283, 'admin', 'Login', '2015-12-19 01:50:50'),
(284, 'admin', 'UpdateDetail', '2015-12-19 01:51:03'),
(285, 'admin', 'UpdateGroup', '2015-12-19 01:51:11'),
(286, '', 'Reject Form Promotion', '2015-12-19 01:55:18'),
(287, 'admin', 'Login', '2015-12-19 01:55:24'),
(288, '', 'Reject Form Promotion', '2015-12-19 02:02:00'),
(289, 'admin', 'Login', '2015-12-19 02:02:05'),
(290, 'admin', 'UpdateDetail', '2015-12-19 02:28:24'),
(291, 'admin', 'UpdateGroup', '2015-12-19 02:28:30'),
(292, 'admin', 'UpdateGroup', '2015-12-19 02:28:36'),
(293, 'admin', 'UpdateDetail', '2015-12-19 02:29:36'),
(294, 'admin', 'UpdateGroup', '2015-12-19 02:29:42'),
(295, 'admin', 'UpdateGroup', '2015-12-19 02:29:49'),
(296, 'admin', 'UpdateGroup', '2015-12-19 02:35:42'),
(297, 'admin', 'UpdateDetail', '2015-12-19 02:35:51'),
(298, 'admin', 'UpdateGroup', '2015-12-19 02:35:57'),
(299, 'admin', 'UpdateDetail', '2015-12-19 02:38:22'),
(300, 'admin', 'UpdateGroup', '2015-12-19 02:38:34'),
(301, 'admin', 'UpdateGroup', '2015-12-19 02:38:42'),
(302, 'admin', 'Login', '2015-12-19 02:59:05'),
(303, 'admin', 'UpdateDetail', '2015-12-19 05:14:24'),
(304, 'admin', 'UpdateGroup', '2015-12-19 05:14:30'),
(305, 'admin', 'UpdateGroup', '2015-12-19 05:37:17'),
(306, 'admin', 'UpdateGroup', '2015-12-19 05:37:27'),
(307, '', 'Reject Form Beacon', '2015-12-20 17:49:05'),
(308, 'admin', 'Login', '2015-12-20 17:49:10');

-- --------------------------------------------------------

--
-- 資料表結構 `androidusers`
--

CREATE TABLE `androidusers` (
  `uid` int(11) NOT NULL,
  `unique_id` varchar(11) NOT NULL,
  `firstname` text NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `androidusers`
--

INSERT INTO `androidusers` (`uid`, `unique_id`, `firstname`, `lastname`, `username`, `email`, `encrypted_password`, `salt`, `created_at`) VALUES
(1, '', '', '', 'single', 'singleqiang@gmail.com', '', '', NULL),
(2, '56745d6880a', '王', '昭程', 'qqqqq', 'qwe@yahoo.com', 'd40IbhyO8HJaX28GvRORHQaMhjU2MzQzODY4NzE1', '6343868715', '2015-12-19 03:24:24'),
(3, '56745d98332', '陳', '暐婷', 'wwwww', 'asd@yahoo.com', 'ARiYA1nZ5W5CJR4EhQsMrIX6R281YjNiNjgzMGM5', '5b3b6830c9', '2015-12-19 03:25:12'),
(4, '56745dbbcf9', '洪', '梓翔', 'eeeee', 'zxc@yahoo.com', 'h/Aeml6DiZ3zjqMwCN9ySG6NvzczZTQ4OTQ2Y2Q2', '3e48946cd6', '2015-12-19 03:25:47'),
(5, '56745dd6132', '蔡', '一謙', 'aaaaa', 'qaz@yahoo.com', 'Gww3OTc83halkUpcdTYOF/D0X+BiMzc1N2U3M2I0', 'b3757e73b4', '2015-12-19 03:26:14');

-- --------------------------------------------------------

--
-- 資料表結構 `beacon`
--

CREATE TABLE `beacon` (
  `Id` int(10) NOT NULL,
  `Major` int(10) NOT NULL,
  `Minor` int(10) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Locate` varchar(50) NOT NULL,
  `Note` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `beacon`
--

INSERT INTO `beacon` (`Id`, `Major`, `Minor`, `Name`, `Locate`, `Note`) VALUES
(1, 10507, 7590, 'estimote 1', '1桌', ''),
(2, 12103, 5060, 'estimote new', '2桌', ''),
(3, 6650, 20240, 'iphone 1000', '3桌', ''),
(4, 41084, 35657, 'Estimote 紫', '4桌', ''),
(5, 53419, 1, 'Beacon 綠', '5桌', ''),
(6, 15155, 41016, 'Beacon 淺藍', '6桌', '');

-- --------------------------------------------------------

--
-- 資料表結構 `menuproduct`
--

CREATE TABLE `menuproduct` (
  `Id` int(10) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `price` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Shop` varchar(50) NOT NULL,
  `ShopId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `menuproduct`
--

INSERT INTO `menuproduct` (`Id`, `Name`, `type`, `price`, `Shop`, `ShopId`) VALUES
(1, '魷魚石鍋拌飯', ' 飯', '180', '韓鶴亭', 1),
(2, '豆腐湯鍋套餐', '飯', '180', '韓鶴亭', 1),
(3, '烤肉石鍋拌飯', ' 飯', '180', '韓鶴亭', 1),
(4, '韓式拌飯', ' 飯', '140', '韓鶴亭', 1),
(5, '香菇石鍋拌飯', ' 飯', '180', '韓鶴亭', 1),
(6, '紅酒牛肉歐姆蛋', '', '150', 'GUGU廚房', 2),
(7, '豬排義大利麵', '麵', '150', 'GUGU廚房', 2),
(8, '酥皮濃湯', '湯', '150', 'GUGU廚房', 2),
(9, '豬排焗烤飯', ' 飯', '150', 'GUGU廚房', 2),
(10, '花枝義大利麵', '麵', '170', 'GUGU廚房', 2),
(11, '卡滋雪花豬排丼', '飯', '139', '鮮五丼', 3),
(12, '咖哩雪花豬排丼', '飯', '129', '鮮五丼', 3),
(13, '香烤雞腿丼', '飯', '129', '鮮五丼', 3),
(14, '牛肉烏龍麵', '麵', '159', '鮮五丼', 3),
(15, '半筋半肉牛肉麵', '麵', '115', '三商巧福', 4),
(16, '沙茶肉燥乾拌麵', '麵', '72', '三商巧福', 4),
(17, '超大盛牛肉壽喜飯', '飯', '115', '三商巧福', 4),
(18, '紅燒排骨飯', '飯', '99', '三商巧福', 4),
(19, ' 日式醬燒雞肉', '飯', '380', '翰林茶館', 5),
(20, '糖醋鮮魚', '', '380', '翰林茶館', 5),
(21, '素食牛奶鍋', '', '320', '翰林茶館', 5),
(22, '熊貓鮮奶茶', '', '140', '翰林茶館', 5);

-- --------------------------------------------------------

--
-- 資料表結構 `msgboard`
--

CREATE TABLE `msgboard` (
  `Id` int(10) NOT NULL,
  `Reply` int(11) NOT NULL DEFAULT '0',
  `Subject` varchar(50) NOT NULL,
  `Context` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `msgboard`
--

INSERT INTO `msgboard` (`Id`, `Reply`, `Subject`, `Context`, `Email`, `TIME`) VALUES
(15, 0, 'Question of ABC', 'hello ', 'name@mail.com', '2015-12-16 00:01:42');

-- --------------------------------------------------------

--
-- 資料表結構 `ordergroup`
--

CREATE TABLE `ordergroup` (
  `Id` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `CustomerEmail` varchar(50) NOT NULL,
  `Total_price` int(11) NOT NULL,
  `Locate` varchar(10) DEFAULT NULL,
  `Shop` varchar(10) NOT NULL,
  `ShopId` varchar(10) NOT NULL,
  `CeateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Purchase` int(11) NOT NULL DEFAULT '0',
  `Complete` int(11) NOT NULL DEFAULT '0',
  `BeaconId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `ordergroup`
--

INSERT INTO `ordergroup` (`Id`, `CustomerId`, `CustomerEmail`, `Total_price`, `Locate`, `Shop`, `ShopId`, `CeateTime`, `Purchase`, `Complete`, `BeaconId`) VALUES
(1, 2, 'qwe@yahoo.com', 360, NULL, '韓鶴亭', '1', '2015-12-18 19:29:29', 1, 1, 0),
(2, 2, 'qwe@yahoo.com', 300, NULL, 'GUGU廚房', '2', '2015-12-18 19:29:37', 1, 1, 0),
(3, 2, 'qwe@yahoo.com', 187, NULL, '三商巧福', '4', '2015-12-18 19:29:43', 1, 1, 0),
(4, 3, 'asd@yahoo.com', 760, NULL, '翰林茶館', '5', '2015-12-18 19:30:56', 1, 1, 0),
(8, 4, 'zxc@yahoo.com', 520, NULL, 'GUGU廚房', '2', '2015-12-18 19:42:56', 1, 1, 0),
(9, 5, 'qaz@yahoo.com', 670, NULL, 'GUGU廚房', '2', '2015-12-18 19:43:17', 1, 1, 0),
(10, 5, 'qaz@yahoo.com', 670, NULL, 'GUGU廚房', '2', '2015-12-18 19:43:22', 1, 1, 0),
(11, 5, 'qaz@yahoo.com', 150, NULL, 'GUGU廚房', '2', '2015-12-18 19:43:29', 1, 1, 0),
(12, 2, 'qwe@yahoo.com', 245, NULL, 'GUGU廚房', '2', '2015-12-18 19:44:12', 1, 1, 1),
(13, 2, 'qwe@yahoo.com', 252, NULL, '韓鶴亭', '1', '2015-12-18 19:44:23', 1, 1, 1),
(14, 2, 'qwe@yahoo.com', 322, NULL, '韓鶴亭', '1', '2015-12-18 19:44:31', 1, 1, 1),
(15, 2, 'qwe@yahoo.com', 252, NULL, '韓鶴亭', '1', '2015-12-18 19:44:38', 1, 1, 1),
(16, 2, 'qwe@yahoo.com', 252, NULL, '韓鶴亭', '1', '2015-12-18 19:45:40', 1, 1, 1),
(17, 2, 'qwe@yahoo.com', 322, NULL, '韓鶴亭', '1', '2015-12-18 19:46:06', 1, 1, 1),
(18, 2, 'qwe@yahoo.com', 490, NULL, '翰林茶館', '5', '2015-12-18 19:46:51', 1, 1, 1),
(19, 2, 'qwe@yahoo.com', 266, NULL, '翰林茶館', '5', '2015-12-18 19:46:59', 1, 1, 1),
(20, 5, 'qaz@yahoo.com', 490, NULL, '翰林茶館', '5', '2015-12-18 19:47:27', 1, 1, 1),
(21, 5, 'qaz@yahoo.com', 224, NULL, '翰林茶館', '5', '2015-12-18 19:48:09', 1, 1, 1),
(22, 5, 'qaz@yahoo.com', 103, NULL, '三商巧福', '4', '2015-12-18 19:48:49', 1, 1, 1),
(23, 5, 'qaz@yahoo.com', 168, NULL, '三商巧福', '4', '2015-12-18 19:48:57', 1, 1, 1),
(24, 5, 'qaz@yahoo.com', 192, NULL, '三商巧福', '4', '2015-12-18 19:49:07', 1, 1, 1),
(25, 5, 'qaz@yahoo.com', 324, NULL, '韓鶴亭', '1', '2015-12-18 21:20:17', 1, 1, 1),
(26, 2, 'qwe@yahoo.com', 224, NULL, '翰林茶館', '5', '2015-12-19 01:50:24', 1, 1, 1),
(27, 2, 'qwe@yahoo.com', 266, NULL, '翰林茶館', '5', '2015-12-19 02:28:07', 1, 1, 5),
(28, 2, 'qwe@yahoo.com', 180, NULL, '韓鶴亭', '1', '2015-12-19 02:29:22', 1, 1, 0),
(29, 2, 'qwe@yahoo.com', 180, NULL, '韓鶴亭', '1', '2015-12-19 02:35:29', 1, 1, 4),
(30, 2, 'qwe@yahoo.com', 180, NULL, '韓鶴亭', '1', '2015-12-19 02:38:10', 1, 1, 0),
(31, 5, 'qaz@yahoo.com', 266, NULL, '翰林茶館', '5', '2015-12-19 05:14:04', 1, 1, 4),
(32, 2, 'qwe@yahoo.com', 240, NULL, 'GUGU廚房', '2', '2015-12-19 05:36:42', 1, 1, 1),
(33, 2, 'qwe@yahoo.com', 126, NULL, '韓鶴亭', '1', '2015-12-19 05:38:28', 0, 0, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `orderlist`
--

CREATE TABLE `orderlist` (
  `Id` int(10) NOT NULL,
  `GroupId` varchar(10) NOT NULL,
  `Shop` varchar(50) CHARACTER SET utf8mb4 DEFAULT '翰林茶館',
  `Product` varchar(50) NOT NULL DEFAULT '熊貓鮮奶茶',
  `Count` int(10) NOT NULL DEFAULT '1',
  `Price` int(10) NOT NULL DEFAULT '199',
  `CeateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CustomerEmail` varchar(50) NOT NULL DEFAULT 'test@mail.com',
  `BeaconId` int(10) NOT NULL DEFAULT '1',
  `ProductId` int(10) NOT NULL DEFAULT '22',
  `ShopId` int(10) NOT NULL DEFAULT '5',
  `CustomerId` int(10) NOT NULL DEFAULT '0',
  `Complete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `orderlist`
--

INSERT INTO `orderlist` (`Id`, `GroupId`, `Shop`, `Product`, `Count`, `Price`, `CeateTime`, `CustomerEmail`, `BeaconId`, `ProductId`, `ShopId`, `CustomerId`, `Complete`) VALUES
(1, '1', '韓鶴亭', '魷魚石鍋拌飯', 1, 180, '2015-12-18 19:29:29', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(2, '1', '韓鶴亭', '豆腐湯鍋套餐', 1, 180, '2015-12-18 19:29:29', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(3, '2', 'GUGU廚房', '紅酒牛肉歐姆蛋', 1, 150, '2015-12-18 19:29:37', 'qwe@yahoo.com', 1, 22, 2, 2, 1),
(4, '2', 'GUGU廚房', '豬排義大利麵', 1, 150, '2015-12-18 19:29:37', 'qwe@yahoo.com', 1, 22, 2, 2, 1),
(5, '3', '三商巧福', '半筋半肉牛肉麵', 1, 115, '2015-12-18 19:29:43', 'qwe@yahoo.com', 1, 22, 4, 2, 1),
(6, '3', '三商巧福', '沙茶肉燥乾拌麵', 1, 72, '2015-12-18 19:29:43', 'qwe@yahoo.com', 1, 22, 4, 2, 1),
(7, '4', '翰林茶館', '日式醬燒雞肉', 1, 380, '2015-12-18 19:31:00', 'asd@yahoo.com', 1, 22, 5, 3, 1),
(8, '4', '翰林茶館', '糖醋鮮魚', 1, 380, '2015-12-18 19:31:00', 'asd@yahoo.com', 1, 22, 5, 3, 1),
(9, '5', '鮮五丼', '咖哩雪花豬排丼', 1, 129, '2015-12-18 19:31:57', 'asd@yahoo.com', 1, 22, 3, 3, 1),
(10, '5', '鮮五丼', '香烤雞腿丼', 1, 129, '2015-12-18 19:31:57', 'asd@yahoo.com', 1, 22, 3, 3, 1),
(11, '6', 'GUGU廚房', '紅酒牛肉歐姆蛋', 1, 150, '2015-12-18 19:42:30', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(12, '6', 'GUGU廚房', '豬排義大利麵', 1, 150, '2015-12-18 19:42:30', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(13, '6', 'GUGU廚房', '酥皮濃湯', 1, 50, '2015-12-18 19:42:30', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(14, '6', 'GUGU廚房', '豬排焗烤飯', 1, 150, '2015-12-18 19:42:30', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(15, '6', 'GUGU廚房', '花枝義大利麵', 1, 170, '2015-12-18 19:42:30', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(16, '7', '鮮五丼', '卡滋雪花豬排丼', 1, 139, '2015-12-18 19:42:45', 'zxc@yahoo.com', 1, 22, 3, 4, 1),
(17, '7', '鮮五丼', '咖哩雪花豬排丼', 1, 129, '2015-12-18 19:42:45', 'zxc@yahoo.com', 1, 22, 3, 4, 1),
(18, '7', '鮮五丼', '香烤雞腿丼', 1, 129, '2015-12-18 19:42:45', 'zxc@yahoo.com', 1, 22, 3, 4, 1),
(19, '7', '鮮五丼', '牛肉烏龍麵', 1, 159, '2015-12-18 19:42:45', 'zxc@yahoo.com', 1, 22, 3, 4, 1),
(20, '8', 'GUGU廚房', '豬排義大利麵', 1, 150, '2015-12-18 19:42:57', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(21, '8', 'GUGU廚房', '酥皮濃湯', 1, 50, '2015-12-18 19:42:57', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(22, '8', 'GUGU廚房', '豬排焗烤飯', 1, 150, '2015-12-18 19:42:57', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(23, '8', 'GUGU廚房', '花枝義大利麵', 1, 170, '2015-12-18 19:42:57', 'zxc@yahoo.com', 1, 22, 2, 4, 1),
(24, '9', 'GUGU廚房', '紅酒牛肉歐姆蛋', 1, 150, '2015-12-18 19:43:23', 'qaz@yahoo.com', 1, 22, 2, 5, 1),
(25, '9', 'GUGU廚房', '豬排義大利麵', 1, 150, '2015-12-18 19:43:23', 'qaz@yahoo.com', 1, 22, 2, 5, 1),
(26, '9', 'GUGU廚房', '酥皮濃湯', 1, 50, '2015-12-18 19:43:23', 'qaz@yahoo.com', 1, 22, 2, 5, 1),
(27, '9', 'GUGU廚房', '豬排焗烤飯', 1, 150, '2015-12-18 19:43:23', 'qaz@yahoo.com', 1, 22, 2, 5, 1),
(28, '9', 'GUGU廚房', '花枝義大利麵', 1, 170, '2015-12-18 19:43:23', 'qaz@yahoo.com', 1, 22, 2, 5, 1),
(29, '11', 'GUGU廚房', '豬排義大利麵', 1, 150, '2015-12-18 19:43:29', 'qaz@yahoo.com', 1, 22, 2, 5, 1),
(30, '12', 'GUGU廚房', '紅酒牛肉歐姆蛋', 1, 150, '2015-12-18 19:44:16', 'qwe@yahoo.com', 1, 22, 2, 2, 1),
(31, '12', 'GUGU廚房', '豬排義大利麵', 1, 150, '2015-12-18 19:44:16', 'qwe@yahoo.com', 1, 22, 2, 2, 1),
(32, '12', 'GUGU廚房', '酥皮濃湯', 1, 50, '2015-12-18 19:44:16', 'qwe@yahoo.com', 1, 22, 2, 2, 1),
(33, '13', '韓鶴亭', '魷魚石鍋拌飯', 1, 180, '2015-12-18 19:44:23', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(34, '13', '韓鶴亭', '豆腐湯鍋套餐', 1, 180, '2015-12-18 19:44:23', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(35, '14', '韓鶴亭', '烤肉石鍋拌飯', 1, 180, '2015-12-18 19:44:31', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(36, '14', '韓鶴亭', '韓式拌飯', 1, 140, '2015-12-18 19:44:31', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(37, '14', '韓鶴亭', '香菇石鍋拌飯', 1, 140, '2015-12-18 19:44:31', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(38, '15', '韓鶴亭', '豆腐湯鍋套餐', 1, 180, '2015-12-18 19:44:38', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(39, '15', '韓鶴亭', '烤肉石鍋拌飯', 1, 180, '2015-12-18 19:44:38', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(40, '16', '韓鶴亭', '豆腐湯鍋套餐', 1, 180, '2015-12-18 19:45:40', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(41, '16', '韓鶴亭', '烤肉石鍋拌飯', 1, 180, '2015-12-18 19:45:40', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(42, '17', '韓鶴亭', '烤肉石鍋拌飯', 1, 180, '2015-12-18 19:46:06', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(43, '17', '韓鶴亭', '韓式拌飯', 1, 140, '2015-12-18 19:46:06', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(44, '17', '韓鶴亭', '香菇石鍋拌飯', 1, 140, '2015-12-18 19:46:06', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(45, '18', '翰林茶館', '糖醋鮮魚', 1, 380, '2015-12-18 19:46:51', 'qwe@yahoo.com', 1, 22, 5, 2, 1),
(46, '18', '翰林茶館', '素食牛奶鍋', 1, 320, '2015-12-18 19:46:51', 'qwe@yahoo.com', 1, 22, 5, 2, 1),
(47, '19', '翰林茶館', '日式醬燒雞肉', 1, 380, '2015-12-18 19:46:59', 'qwe@yahoo.com', 1, 22, 5, 2, 1),
(48, '20', '翰林茶館', '糖醋鮮魚', 1, 380, '2015-12-18 19:47:39', 'qaz@yahoo.com', 1, 22, 5, 5, 1),
(49, '20', '翰林茶館', '素食牛奶鍋', 1, 320, '2015-12-18 19:47:39', 'qaz@yahoo.com', 1, 22, 5, 5, 1),
(50, '21', '翰林茶館', '素食牛奶鍋', 1, 320, '2015-12-18 19:48:09', 'qaz@yahoo.com', 1, 22, 5, 5, 1),
(51, '22', '三商巧福', '超大盛牛肉壽喜飯', 1, 115, '2015-12-18 19:48:49', 'qaz@yahoo.com', 1, 22, 4, 5, 1),
(52, '23', '三商巧福', '半筋半肉牛肉麵', 1, 115, '2015-12-18 19:48:57', 'qaz@yahoo.com', 1, 22, 4, 5, 1),
(53, '23', '三商巧福', '沙茶肉燥乾拌麵', 1, 72, '2015-12-18 19:48:57', 'qaz@yahoo.com', 1, 22, 4, 5, 1),
(54, '24', '三商巧福', '超大盛牛肉壽喜飯', 1, 115, '2015-12-18 19:49:09', 'qaz@yahoo.com', 1, 22, 4, 5, 1),
(55, '24', '三商巧福', '紅燒排骨飯', 1, 99, '2015-12-18 19:49:09', 'qaz@yahoo.com', 1, 22, 4, 5, 1),
(56, '25', '韓鶴亭', '魷魚石鍋拌飯', 1, 180, '2015-12-18 21:20:17', 'qaz@yahoo.com', 1, 22, 1, 5, 1),
(57, '25', '韓鶴亭', '豆腐湯鍋套餐', 1, 180, '2015-12-18 21:20:18', 'qaz@yahoo.com', 1, 22, 1, 5, 1),
(58, '26', '翰林茶館', '素食牛奶鍋', 1, 320, '2015-12-19 01:50:24', 'qwe@yahoo.com', 1, 22, 5, 2, 1),
(59, '27', '翰林茶館', '糖醋鮮魚', 1, 380, '2015-12-19 02:28:08', 'qwe@yahoo.com', 1, 22, 5, 2, 1),
(60, '28', '韓鶴亭', '魷魚石鍋拌飯', 1, 180, '2015-12-19 02:29:23', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(61, '29', '韓鶴亭', '魷魚石鍋拌飯', 1, 180, '2015-12-19 02:35:29', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(62, '30', '韓鶴亭', '魷魚石鍋拌飯', 1, 180, '2015-12-19 02:38:11', 'qwe@yahoo.com', 1, 22, 1, 2, 1),
(63, '31', '翰林茶館', '日式醬燒雞肉', 1, 380, '2015-12-19 05:14:04', 'qaz@yahoo.com', 1, 22, 5, 5, 1),
(64, '32', 'GUGU廚房', '紅酒牛肉歐姆蛋', 1, 150, '2015-12-19 05:36:42', 'qwe@yahoo.com', 1, 22, 2, 2, 0),
(65, '32', 'GUGU廚房', '豬排義大利麵', 1, 150, '2015-12-19 05:36:42', 'qwe@yahoo.com', 1, 22, 2, 2, 0),
(66, '33', '韓鶴亭', '烤肉石鍋拌飯', 1, 180, '2015-12-19 05:38:28', 'qwe@yahoo.com', 1, 22, 1, 2, 0);

-- --------------------------------------------------------

--
-- 資料表結構 `promotions`
--

CREATE TABLE `promotions` (
  `Id` int(10) NOT NULL,
  `Name` varchar(50) NOT NULL DEFAULT '優惠名稱',
  `Context` varchar(50) NOT NULL DEFAULT '優惠內容',
  `BeaconId` int(10) NOT NULL DEFAULT '1',
  `Shop` varchar(50) NOT NULL DEFAULT '1',
  `Product` varchar(50) NOT NULL DEFAULT 'Product1',
  `Product2` varchar(50) NOT NULL DEFAULT 'Product2',
  `Product3` varchar(50) NOT NULL DEFAULT 'Product3 ',
  `Product4` varchar(50) NOT NULL DEFAULT 'Product4',
  `SalePrice` int(10) NOT NULL DEFAULT '499',
  `ShopId` int(10) NOT NULL DEFAULT '1',
  `Note` varchar(50) NOT NULL DEFAULT 'note',
  `TIME` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `promotions`
--

INSERT INTO `promotions` (`Id`, `Name`, `Context`, `BeaconId`, `Shop`, `Product`, `Product2`, `Product3`, `Product4`, `SalePrice`, `ShopId`, `Note`, `TIME`) VALUES
(1, '優惠店家名', '優惠內容', 1, '請選擇', 'Product1', 'Product2', 'Product3 ', 'Product4', 1, 1, 'note', '00:01~23:59'),
(2, '韓鶴亭', '聖誕節 全面7折', 2, '三商巧福', '', '', '', '', 7, 1, '', '00:01~23:59'),
(3, 'GUGU廚房', '限時大優惠 全面8折', 3, 'GUGU廚房', '', '', '', '', 8, 2, '', '00:01~23:59');

-- --------------------------------------------------------

--
-- 資料表結構 `shop`
--

CREATE TABLE `shop` (
  `Id` int(10) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Locate` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Capacity` int(10) NOT NULL,
  `Area` int(10) NOT NULL,
  `BeaconUse` int(10) NOT NULL,
  `MenuId` int(10) NOT NULL,
  `Person` varchar(50) NOT NULL,
  `Contact` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Note` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `shop`
--

INSERT INTO `shop` (`Id`, `Name`, `Locate`, `Capacity`, `Area`, `BeaconUse`, `MenuId`, `Person`, `Contact`, `Note`) VALUES
(1, '韓鶴亭', 'r1', 0, 0, 0, 0, '韓先生', '03-1234567', ''),
(2, 'GUGU廚房', 'r2', 0, 0, 0, 0, 'GG人', '03-144234', ''),
(3, '鮮五丼', 'r3', 0, 0, 0, 0, '牛先生', '03-1234567', ''),
(4, '三商巧福', 'r4', 0, 0, 0, 0, '三商企業', '04-1234567', ''),
(5, '翰林茶館', 'r5', 0, 0, 0, 0, '', '', ''),
(6, '', '', 0, 0, 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', '123', 'admin'),
(3, 'order', '123', 'order'),
(4, 'shop', '123', 'shop'),
(5, 'menu', '123', 'menu'),
(6, 'data', '123', 'data'),
(7, 'bea', '123', 'bea'),
(8, 'pro', '123', 'pro');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `accountlog`
--
ALTER TABLE `accountlog`
  ADD PRIMARY KEY (`Id`);

--
-- 資料表索引 `androidusers`
--
ALTER TABLE `androidusers`
  ADD PRIMARY KEY (`uid`);

--
-- 資料表索引 `beacon`
--
ALTER TABLE `beacon`
  ADD UNIQUE KEY `Id` (`Id`);

--
-- 資料表索引 `menuproduct`
--
ALTER TABLE `menuproduct`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id` (`Id`),
  ADD KEY `ShopId` (`ShopId`);

--
-- 資料表索引 `msgboard`
--
ALTER TABLE `msgboard`
  ADD PRIMARY KEY (`Id`);

--
-- 資料表索引 `ordergroup`
--
ALTER TABLE `ordergroup`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`);

--
-- 資料表索引 `orderlist`
--
ALTER TABLE `orderlist`
  ADD UNIQUE KEY `Id` (`Id`),
  ADD KEY `ShopId` (`ShopId`),
  ADD KEY `OrderGroup` (`GroupId`);

--
-- 資料表索引 `promotions`
--
ALTER TABLE `promotions`
  ADD UNIQUE KEY `Id` (`Id`);

--
-- 資料表索引 `shop`
--
ALTER TABLE `shop`
  ADD UNIQUE KEY `Id` (`Id`);

--
-- 資料表索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `accountlog`
--
ALTER TABLE `accountlog`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=309;
--
-- 使用資料表 AUTO_INCREMENT `androidusers`
--
ALTER TABLE `androidusers`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用資料表 AUTO_INCREMENT `beacon`
--
ALTER TABLE `beacon`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用資料表 AUTO_INCREMENT `menuproduct`
--
ALTER TABLE `menuproduct`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- 使用資料表 AUTO_INCREMENT `msgboard`
--
ALTER TABLE `msgboard`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- 使用資料表 AUTO_INCREMENT `ordergroup`
--
ALTER TABLE `ordergroup`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- 使用資料表 AUTO_INCREMENT `orderlist`
--
ALTER TABLE `orderlist`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- 使用資料表 AUTO_INCREMENT `promotions`
--
ALTER TABLE `promotions`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- 使用資料表 AUTO_INCREMENT `shop`
--
ALTER TABLE `shop`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用資料表 AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
